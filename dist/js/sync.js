	var socket = io.connect('ws://dsl.urusai.ninja:182/poke');
	socket.emit('user connect', { username: name, room: room });
	
	$('#ctl_play').on('click', function() {
		// $('#media')[0].play();
		socket.emit('interact', { action: 'play', room: room });
		socket.emit('chat message', { message: '(resumed)', username: name, room: room });
	});
	$('#ctl_pause').on('click', function() {
		// $('#media')[0].pause();
		socket.emit('interact', { action: 'pause', room: room });
		socket.emit('chat message', { message: '(paused)', username: name, room: room });
	});
	
	$('#ctl_vol_mute').on('click', function() {
		media_vol = 0;
		setVolume(0);
	});
	$('#ctl_vol_down').on('click', function() {
		if (media_vol > 0) {
			media_vol -= 1;
			setVolume(media_vol);
		}
	});
	$('#ctl_vol_up').on('click', function() {
		if (media_vol < 10) {
			media_vol += 1;
			setVolume(media_vol);
		}
	});
	
	$('#textinput').on('keydown', function(e) {
		if (e.which == 13) {
			strlc = $('#textinput').val();
			switch (strlc) {
				case '/resync':
					socket.emit('chat message', { message: '(resyncing...)', username: name, room: room });
					socket.emit('interact', { action: 'seek', value: $('#media')[0].currentTime, room: room });
				break;
				case '/load':
					url = prompt('URL to video?');
					if (url.length > 5) {
						if (url.lastIndexOf(';hd=') > 0) {
							var sd = url.substring(0, url.lastIndexOf(';hd='));
							var hd = url.substring(url.lastIndexOf(';hd=') + 4);
							socket.emit('interact', { action: 'url', value: sd, hdvalue: hd, room: room });
							var bn = sd.substring(sd.lastIndexOf('/') + 1);
							bn = bn.replace(/%20/g, ' ').replace(/\\[.*?\\]/g, '');
							bn = bn.substring(0, bn.lastIndexOf('.'));
							//socket.emit('chat message', { message: name + ' loaded a new video: ' + bn + 'sd: ' + sd + ',hd: ' + hd, username: '[S]', room: room, skipfilter: true });
							socket.emit('chat message', { message: name + ' loaded a new video: ' + bn, username: '[S]', room: room, skipfilter: true });
						}
						else {
							socket.emit('interact', { action: 'url', value: url, room: room });
							var bn = url.substring(url.lastIndexOf('/') + 1);
							bn = bn.replace(/%20/g, ' ').replace(/\\[.*?\\]/g, '');
							bn = bn.substring(0, bn.lastIndexOf('.'));
							socket.emit('chat message', { message: name + ' loaded a new video: ' + bn, username: '[S]', room: room, skipfilter: true });
						}
					}
				break;
				case '/mute':
					media_vol = 0;
					setVolume(0);
				break;
				case '/c':
				case '/clean':
				case '/clear':
					$('#log li').remove();
				break;
				case '/on':
					$('#log').show();
				break;
				case '/off':
					$('#log').hide();
				break;
				case '/skipop':
					// pause
					socket.emit('interact', { action: 'pause', room: room });
					// advance by 1:25
					socket.emit('interact', { action: 'seek', value: $('#media')[0].currentTime + 85, room: room });
					// alert
					socket.emit('chat message', { message: '(skipped opening)', username: name, room: room });
				break;
				case '/?':
					displayLogHTML('<br/>Commands:<br/>/? - help<br/>/mute - quick mute<br/>/c|clean|clear - clear chat log<br/>/on | /off - turn chat log on|off<br/>/resync - resync everyone to my position<br/>/beepon|off - enable/disable beep<br/><br/>', false);
				break;
				case '/hd':
					prefer_hd = true;
					displayLogHTML('<br/>[HD enabled]<br/>', false);
					hLoadMedia();
				break;
				case '/sd':
					prefer_hd = false;
					displayLogHTML('<br/>[HD disabled]<br/>', false);
					hLoadMedia();
				break;
				case '/readycheck':
					socket.emit('interact', { action: 'ready-check', room: room });
					socket.emit('chat message', { message: '(started a ready check)', username: name, room: room });
				break;
				case '/beepon':
					$('#beep')[0].volume = 0.75;
					displayLogHTML('<br/>[beep enabled]<br/>', false);
				break;
				case '/beepoff':
					$('#beep')[0].volume = 0;
					displayLogHTML('<br/>[beep disabled]<br/>', false);
				break;
				default:
					if (strlc.length > 0) {
						socket.emit('chat message', { message: strlc, username: name, room: room });
						displayLog($('<div>').text(strlc).html(), name);
					}
				break;
			}
			$('#textinput').val('');
		}
	});
	
	//
	
	socket.on('chat message', function(e) {
		if (e.room != room) return;
		if (e.username == name) return;
		displayPop(e.message, e.username);
		displayLog(e.message, e.username);
	});
	socket.on('interact', function(e) {
		if (e.room != room) return;
		if (e.action == 'play') {
			$('#media')[0].play();
		}
		if (e.action == 'pause') {
			$('#media')[0].pause();
		}
		if (e.action == 'seek') {
			$('#media')[0].currentTime = e.value;
		}
		if (e.action == 'url') {
			if (prefer_hd && e.hdvalue) {
				$('#media')[0].src = e.hdvalue;
				displayLogHTML('<br/>[Loaded HD video]<br/>', false);
			}
			else {
				$('#media')[0].src = e.value;
			}
			initBufferWatcher();
			$('#media')[0].play();
			$('#media')[0].pause();
		}
		if (e.action == 'url-mp') {
			if (prefer_hd) {
				if (e.value.src_high != undefined && e.value.src_high != '') {
					url = e.value.src_high;
					displayLogHTML('<br/>[Loaded HD video]<br/>', false);
				}
				else if (e.value.src_low != undefined && e.value.src_low != '') {
					url = e.value.src_low;
					displayLogHTML('<br/>[Loaded SD video]<br/>', false);
				}
			}
			else {
				if (e.value.src_low != undefined && e.value.src_low != '') {
					url = e.value.src_low;
					if (e.value.src_high != undefined && e.value.src_high != '') {
						displayLogHTML('<br/>[Loaded SD video -- HD available, type /hd]<br/>', false);
					}
					else {
						displayLogHTML('<br/>[Loaded SD video]<br/>', false);
					}
				}
				else if (e.value.src_high != undefined && e.value.src_high != '') {
					url = e.value.src_high;
					displayLogHTML('<br/>[Loaded HD video]<br/>', false);
				}
			}
			if ($('#media')[0].src != url) $('#media')[0].src = url;
			
			initBufferWatcher();
			$('#media')[0].play();
			$('#media')[0].pause();
		}
		if (e.action == 'h-anime-title-change') {
			//$('#header_anime option:eq(' + e.value + ')').prop('selected', true);
			$('#header_anime').val(e.value);
		}
		if (e.action == 'h-anime-episode-change') {
			//$('#header_episodes option:eq(' + e.value + ')').prop('selected', true);
			$('#header_episodes').val(e.value);
		}
		if (e.action == 'ready-check') {
			$.featherlight('#ready-check');
		}
	});
	
	//
	
	function setVolume(level) {
		$('#media')[0].volume = level / 10.0;
		$('#ctl_vol').html((level * 10) + '%&nbsp;');
	}
	
	function displayPop(msg, auth) {
		$('#textpop_txt').html(msg);
		$('#textpop_auth').text(auth);
		if (!hide_timer) $('#textpop').hide();
		$('#textpop').fadeIn(400);
		hidePopStart();
	}
	function displayLogHTML(msg, auth) {
		if (!auth) {
			$('#log').append($('<li>').html(msg).delay(chat_delay).fadeOut(600));
		}
		else {
			$('#log').append($('<li>').html(auth + ': ' + msg).delay(chat_delay).fadeOut(600));
		}
		$('#textlogctr').scrollTop($('#textlogctr').scrollTop() + $('#log li:last').position().top);
	}
	function displayLog(msg, auth) {
		msg = msg.replace(":[]", "<span style='font-size: 2em;'>&#x1F601;</span>");
		msg = msg.replace(";D", "<span style='font-size: 2em;'>&#x1F602;</span>");
		msg = msg.replace("=D", "<span style='font-size: 2em;'>&#x1F603;</span>");
		msg = msg.replace(":D", "<span style='font-size: 2em;'>&#x1F604;</span>");
		msg = msg.replace(":'D", "<span style='font-size: 2em;'>&#x1F605;</span>");
		msg = msg.replace(">D", "<span style='font-size: 2em;'>&#x1F606;</span>");
		msg = msg.replace(";)", "<span style='font-size: 2em;'>&#x1F609;</span>");
		msg = msg.replace(":)", "<span style='font-size: 2em;'>&#x1F60A;</span>");
		msg = msg.replace(":p", "<span style='font-size: 2em;'>&#x1F60B;</span>");
		msg = msg.replace(":P", "<span style='font-size: 2em;'>&#x1F60B;</span>");
		msg = msg.replace("<3_<3", "<span style='font-size: 2em;'>&#x1F60D;</span>");
		msg = msg.replace(":<3", "<span style='font-size: 2em;'>&#x1F618;</span>");
		msg = msg.replace(">p<", "<span style='font-size: 2em;'>&#x1F61D;</span>");
		msg = msg.replace("=/", "<span style='font-size: 2em;'>&#x1F61E;</span>");
		msg = msg.replace(":o", "<span style='font-size: 2em;'>&#x1F628;</span>");
		msg = msg.replace("T_T", "<span style='font-size: 2em;'>&#x1F62D;</span>");
		msg = msg.replace(":O", "<span style='font-size: 2em;'>&#x1F631;</span>");
		msg = msg.replace("-_-", "<span style='font-size: 2em;'>&#x1F612;</span>");
		msg = msg.replace(":(", "<span style='font-size: 2em;'>&#x1F622;</span>");
		msg = msg.replace(";(", "<span style='font-size: 2em;'>&#x1F62D;</span>");
		if (!auth) {
			$('#log').append($('<li>').html(msg).delay(chat_delay).fadeOut(600));
		}
		else {
			if (msg.substring(0, 5) == '@http') {
				$('#log').append($('<li>').html(auth + ': <a href=\'' + msg.substring(1) + '\' target=\'_blank\'>' + msg.substring(1) + '</a>').delay(chat_delay).fadeOut(600));
			}
			else {
				$('#log').append($('<li>').html(auth + ': ' + msg).delay(chat_delay).fadeOut(600));
				document.title = auth + ': ' + msg;
			}
		}
		$('#textlogctr').scrollTop($('#textlogctr').scrollTop() + $('#log li:last').position().top);
		// sounds??
		$('#beep')[0].play();
	}
	
	function hidePopStart() {
		if (hide_timer) clearTimeout(hide_timer);
		hide_timer = setTimeout(hidePop, 2000);
	}
	function hidePop() {
		$('#textpop').fadeOut(400);
		hide_timer = false;
	}
	
	function timeUpdate() {
		var ctime = formatTime($('#media')[0].currentTime);
		var dtime = formatTime($('#media')[0].duration);
		$('#ctl_time_label').text(
			ctime
			+ ' / ' + 
			dtime
		);
		
		// slider
		$('#ctl_seeker').prop('max', Math.floor($('#media')[0].duration)).prop('value', Math.floor($('#media')[0].currentTime));
	}
	function str_pad_left(string,pad,length){ return (new Array(length+1).join(pad)+string).slice(-length); }
	setInterval(timeUpdate, 500);
	function formatTime(value) {
		var time = Math.floor(value);
		var minutes = Math.floor(time / 60);
		var seconds = time - (minutes * 60);
		var ctime = str_pad_left(minutes,'0',2) + ':' + str_pad_left(seconds,'0',2);
		if (time > (60 * 99) + 59) {
			time = Math.floor($('#media')[0].currentTime);
			var hours = Math.ceil(time / 3600);
			ctime = hours + ':' + ctime;
		}
		return ctime;
	}
	
	//
	
	socket.on('latency', function(e) {
		if (e.room != room) return;
		updateLatency(e.username, e.latency);
		//console.log(e);
	});
	
	socket.on('ping', function(e) {
		socket.emit('pong', { username: name, room: room, start: e.start });
	});
	
	//
	
	function updateLatency(username, latency) {
		// temp
		//username = 'ping';
		
		if (!$('#lat_' + username).length) {
			$('#statusbar').append($('<span>').attr('id', 'lat_' + username).addClass('label label-primary uspan'));
			$('#lat_' + username).append($('<span>').attr('class', 'profile-picture-sm').css('background', 'url(?app=img&do=profile_picture&v=' + username + ')'));
			$('#lat_' + username).append('<br/>');
			$('#lat_' + username).append($('<span>').attr('id', 'lat_text_' + username));
			//$('#lat_' + username).append('<br/>');
			$('#lat_' + username).append($('<span>').attr('id', 'lat_netstat_' + username).text('--').css('font-weight', 'normal').addClass('debug'));
			//$('#lat_' + username).append('<br/>');
			$('#lat_' + username).append($('<span>').attr('id', 'lat_status_' + username).addClass('debug'));
			liveuser[username] = 2;
		}
		$('#lat_text_' + username).text(username + ': ' + latency);
		// some styles.
		if (latency < 120) {
			$('#lat_' + username).removeClass('label-primary label-success label-warning label-danger').addClass('label-info');
		}
		else if (latency < 640) {
			$('#lat_' + username).removeClass('label-success label-warning label-danger label-info').addClass('label-primary');
		}
		else if (latency < 1200) {
			$('#lat_' + username).removeClass('label-success label-danger label-info').addClass('label-warning');
		}
		else {
			$('#lat_' + username).removeClass('label-success label-warning label-info').addClass('label-danger');
		}
		liveuser[username]++;
		if (liveuser[username] > 2) liveuser[username] = 2;
		//$('#lat_ping').text('ping: ' + latency + 'ms');
	}
	setInterval(livetick, 2500);
	function livetick() {
		for (var lu in liveuser) {
			liveuser[lu]--;
			if (liveuser[lu] < 0) {								
				if ($('#lat_' + lu) instanceof Object) {
					$('#lat_' + lu).remove();
				}
			}
		}
		//console.log(liveuser);
	}
	setInterval(statustick, 750);
	function statustick() {
		if ($('#media')[0].paused) {
			stat_player_playing = false;
		}
		else {
			stat_player_playing = true;
		}
		if ($('#textinput').val().length > 0) {
			stat_chat_typing = true;
		}
		else {
			stat_chat_typing = false;
		}
		socket.emit('status', { username: name, room: room, player_loaded: stat_player_loaded, player_playing: stat_player_playing, chat_typing: stat_chat_typing, chat_focused: stat_chat_focused, player_position: $('#media')[0].currentTime, netstat: $('#lat_netstat_' + name).text() });
		//console.log($('#lat_netstat_' + name).text());
	}
	socket.on('status', function(e) {
		if (e.room != room) return;
		$('#lat_status_' + e.username).text('');
		
		if (e.player_loaded) {
			$('#lat_status_' + e.username).append("<span class='fa fa-check' style='color: limegreen;'></span>");
		}
		else {
			$('#lat_status_' + e.username).append("<span class='fa fa-refresh' style='color: orange;'></span>");
		}
		if (e.player_playing) {
			$('#lat_status_' + e.username).append("<span class='fa fa-play' style='color: limegreen;'></span>");
		}
		else {
			$('#lat_status_' + e.username).append("<span class='fa fa-pause'></span>");
		}
		if (e.chat_focused) {
			$('#lat_status_' + e.username).append("<span class='fa fa-eye'></span>");
		}
		else {
			$('#lat_status_' + e.username).append("<span class='fa fa-eye-slash'></span>");
		}
		var pos_diff = e.player_position - $('#media')[0].currentTime;
		if (Math.abs(pos_diff) < 1) {
			$('#lat_status_' + e.username).append("<span class='fa fa-tv'></span>");
		}
		else {
			$('#lat_status_' + e.username).append("<span class='fa fa-tv'> " + Math.round(pos_diff, 2) + "</span>");
		}
		if (e.chat_typing) {
			$('#lat_status_' + e.username).append("<span class='fa fa-commenting'></span>");
		}
		if (e.username !== name) {
			$('#lat_netstat_' + e.username).text(e.netstat);
		}
	});
	
	$(window).focus(function() {
		stat_chat_focused = true;
	}).blur(function() {
		stat_chat_focused = false;
	});
	
	//
	
	$('#media').on('canplay canplaythrough', function(e) {
		stat_player_loaded = true;
	});
	$('#media').on('stalled abort error waiting seeking', function(e) {
		stat_player_loaded = false;
	});
	
	// 
	
	$('.video-wrapper').on('mouseover', function() {
		$('.control').clearQueue().animate({ opacity: 1 }, 500);
	});
	$('.video-wrapper').on('mouseout', function() {
		$('.control').clearQueue().animate({ opacity: 0 }, 500);
	});
	
	//
	
	$(window).bind('beforeunload', function() {
		return 'Are you sure, ' + name + '?';
	});
	$(window).unload(function() {
		socket.emit('chat message', { message: '(left the room)', username: name, room: room });
	});
	
	//
	
	function initBufferWatcher() {
		clearInterval(watchBuffer);
		lastBuffer = 0;
		totalDeltaPS = 0;
		totalDeltaCount = 0;
		fileGFS = 0;
		$.get('?app=urusaisync-ajax&gfs=' + $('#media').prop('src'), function(e) {
			fileGFS = e;
		});
		videoDuration = $('#media').prop('duration');
		watchBuffer = setInterval(updateProgressBar, 500);
	}
	function getReadableFileSizeString(fileSizeInBytes) {

		var i = -1;
		var byteUnits = [' kB', ' MB', ' GB', ' TB', 'PB', 'EB', 'ZB', 'YB'];
		do {
			fileSizeInBytes = fileSizeInBytes / 1024;
			i++;
		} while (fileSizeInBytes > 1024);

		return Math.max(fileSizeInBytes, 0.1).toFixed(1) + byteUnits[i];
	}
	
	var videoDuration;
	var watchBuffer;
	var lastBuffer;
	var lastDeltaPS;
	var totalDeltaCount;
	var fileGFS;

	var updateProgressBar = function(){
		if ($('#media').prop('readyState')) {
			videoDuration = $('#media').prop('duration');
			var buffered = $('#media').prop('buffered').end(0);
			var percent = 100 * buffered / videoDuration;

			//console.log(buffered);
			//console.log(videoDuration);
			//console.log(percent + '%');
			
			var bufferDelta = buffered - lastBuffer;
			var bufferDeltaPS = Math.round(bufferDelta * 2, 4);
			var percent_rounded = Math.round(percent, 2);
			
			bufferChange = (lastDeltaPS + bufferDeltaPS) / 2;
			var avgDeltaPS = Math.round(bufferChange, 4);
			var etaBufferTime = Math.round((videoDuration - buffered) / avgDeltaPS);
			
			if (avgDeltaPS == 0) {
				etaBufferTime = 0;
			}
			var fileBPS = fileGFS / (videoDuration / avgDeltaPS);
			
			
			$('#lat_netstat_' + name).text(etaBufferTime + 's, ' + avgDeltaPS + '/' + bufferDeltaPS + ', ' + percent_rounded + '%, ' + getReadableFileSizeString(fileBPS) + '/s');
			lastBuffer = buffered;
			lastDeltaPS = bufferDeltaPS;
			
			//If finished buffering buffering quit calling it
			if (buffered >= videoDuration) {
				clearInterval(watchBuffer);
				$('#lat_netstat_' + name).text('done');
			}
		}
	};
	$('body').on('click', '#show-debug', function(e) {
		if (this.checked) {
			$('.debug').css('display', 'block');
		}
		else {
			$('.debug').css('display', 'none');
		}
	});
	
	function hUpdateEpisodes() {
		socket.emit('interact', { action: 'h-anime-title-change', value: $('#header_anime').val(), room: room });
		
		$.get('?app=urusaisync2-ajax&getAnimeEpisodes=' + $('#header_anime').val() + '&s_ep=' + s_ep, function(e) {
			$('#header_episodes').html(e);
			if (!first_load_init) {
				$('#media-qload-btn').click();
				first_load_init = true;
			}
		});
		s_ep = 0;
	}
	function hLoadMedia() {
		socket.emit('interact', { action: 'h-anime-episode-change', value: $('#header_episodes').val(), room: room });
		
		$.getJSON('?app=urusaisync2-ajax&loadAnimeEpisode=' + $('#header_anime').val() + '&ep=' + $('#header_episodes').val(), function(e) {
			console.log(e);
			
			socket.emit('interact', { action: 'url-mp', value: e, room: room });
			
			//socket.emit('chat message', { message: name + ' loaded a new video: ' + $('#header_anime option:selected').text() + ' - ' + $('#header_episodes option:selected').text(), username: '[S]', room: room, skipfilter: true });
		});
	}
	$(function() {
		$('#header_anime').on('change', function(e) { hUpdateEpisodes(); });
		$('#media-qload-btn').on('click', function(e) { hLoadMedia(); });
		hUpdateEpisodes();
		$('#ready-check-yes').on('click', function(e) {
			socket.emit('chat message', { message: name + ' is ready!', room: room, skipfilter: true });
			$.featherlight.current().close();
		});
		$('#ready-check-no').on('click', function(e) {
			socket.emit('chat message', { message: name + ' is NOT ready.', room: room, skipfilter: true });
			$.featherlight.current().close();
		});
	});
	
	// slider
	$('#ctl_seeker').mouseup(function() {
		// do seek
		if ($('#media')[0].paused) {
			socket.emit('interact', { action: 'pause', room: room });
			socket.emit('interact', { action: 'seek', value: $(this).val(), room: room });
		}
	});
	// popup message holder
	$('#log li').mouseover(function() {
		$(this).clearQueue(); // don't hide
	});