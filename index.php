<?php
define("ENGINE_PLUGIN_DIR", "e-plugins");
include "config/config.php";
include "engine.php";

$app = "Home";
if (isset($_GET["app"]) && file_exists("bin/{$_GET['app']}.php")) {
	$app = $_GET["app"];
}
if ($app == "widget") {
	header('Access-Control-Allow-Origin: *');
}

if (strc($_SERVER["HTTP_HOST"], "urusai.ninja") && $app !== "UrusaiEdit") define("PAGE_THEME", "urusai");
switch ($app) {
	case "json":
	case "process":
	case "inventory-ajax":
	case "points-ajax":
	case "journal-ajax":
	case "img":
	case "urusai-ajax":
	case "urusaisync-ajax":
	case "urusaisync2-ajax":
	case "notifications-ajax":
	case "tokyo-ajax":
	case "widget":
	case "pAjax":
		include("bin/{$app}.php");
	break;
	case "UrusaiEdit":
		if (!defined("PAGE_THEME")) define("PAGE_THEME", "aftermirror");
		include("page.php");
	break;
	case "UrusaiSync":
	case "UrusaiPlayer":
	case "Urusai":
	case "passthru-u":
		if (!defined("PAGE_THEME")) define("PAGE_THEME", "urusai");
		include("page.php");
		//include("page-urusai.php");
	break;
	default:
		if (!defined("PAGE_THEME")) define("PAGE_THEME", "aftermirror");
		include("page.php");
		//include("page.php");
	break;
}
?>