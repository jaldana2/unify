<?php
ini_set("display_errors", "1");
ini_set("error_reporting", E_ALL);
echo "scraper/start\n";

define("ENGINE_PLUGIN_DIR", "e-plugins");
include "config/config.php";
include "engine.php";
echo "requires included\n";

$site = "http://www.animeultima.io/watch/Boku-dake-ga-Inai-Machi-english-subbed-dubbed-online/";
$fgc = file_get_contents($site);

$dom = new DOMDocument;
libxml_use_internal_errors(true);
$dom->loadHTML($fgc);
$xpath = new DOMXPath($dom);
$nodes = $xpath->query("//td[@class='td-lang-subbed']/a");
$episode = 1;
foreach($nodes as $node) {
	$src = $node->getAttribute('href');
	echo "  src={$src}\n";
	echo "  finding all mirrors...\n";
	$fgc2 = file_get_contents($src);
	$mirrors = array();
	$mirrors[] = $src;
	
	$dom2 = new DOMDocument;
	$dom2->loadHTML($fgc2);
	$xpath2 = new DOMXPath($dom2);
	$nodes2 = $xpath2->query("//div[@class='thumb']/a[@rel='nofollow']");
	foreach ($nodes2 as $node2) {
		$mirrors[] = "http://www.animeultima.io" . $node2->getAttribute("href");
	}
	
	foreach ($mirrors as $mirror) {
		echo "    mirror_src={$mirror}\n";
		$fgc2 = file_get_contents($mirror);
		$dom2->loadHTML($fgc2);
		$xpath2 = new DOMXPath($dom2);
		$nodes2 = $xpath2->query("//div[@id='pembed']/iframe");
		$mirrorurl = $nodes2[0]->getAttribute("src");
		echo "    iframe_src={$mirrorurl}\n";
		$mp4 = false;
		if (strc($mirrorurl, "auengine.com")) {
			$fgc3 = file_get_contents($mirrorurl);
			$fgc3 = substr($fgc3, stripos($fgc3, "video_link") + 14);
			$fgc3 = substr($fgc3, 0, stripos($fgc3, "mp4") + 3);
			$mp4 = $fgc3;
		}
		elseif (strc($mirrorurl, "mp4upload.com")) {
			$fgc3 = file_get_contents($mirrorurl);
			$fgc3 = substr($fgc3, 0, stripos($fgc3, "video.mp4") + 9);
			$fgc3 = substr($fgc3, strripos($fgc3, "http://"));
			$mp4 = $fgc3;
		}
		else {
			echo "    (not supported, skipping)\n";
		}
		if ($mp4) {
			echo "      mp4={$mp4}\n";
			$head = array_change_key_case(get_headers($mp4, TRUE));
			$fs = $head["content-length"];
			$fs2 = formatBytes($fs);
			echo "      fs={$fs} ({$fs2})\n";
			$hd = false;
			if ($fs > 1024*1024*75) {
				$hd = true;
			}
			
		}
	}
	$episode++;
}
?>
