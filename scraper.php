<?php
ini_set("display_errors", "1");
ini_set("error_reporting", E_ALL);
echo "scraper/start\n";

define("ENGINE_PLUGIN_DIR", "e-plugins");
include "config/config.php";
include "engine.php";
echo "requires included\n";
$dlQ = readDB("data/scraper.db");

function curl_get_contents($url) {
	$ch = curl_init();

	$header=array(
	  'User-Agent: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.12) Gecko/20101026 Firefox/3.6.12',
	  'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
	  'Accept-Language: en-us,en;q=0.5',
	  'Accept-Encoding: gzip,deflate',
	  'Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7',
	  'Keep-Alive: 115',
	  'Connection: keep-alive',
	);

	curl_setopt($ch,CURLOPT_URL,$url);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
	curl_setopt($ch,CURLOPT_HTTPHEADER,$header);
	$result=curl_exec($ch);

	curl_close($ch);
	return $result;
}

$opts2 = array(
  'http'=>array(
    'method'=>"GET",
    'header'=>"Connection: keep-alive\r\n" .
			  "Pragma: no-cache\r\n" .
			  "Cache-Control: no-cache\r\n" .
			  "Accept: */*\r\n" .
			  "X-Requested-With: XMLHttpRequest\r\n" .
			  "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.116 Safari/537.36\r\n" .
			  "Referer: http://www.masterani.me/anime/watch/1678-hai-to-gensou-no-grimgar/2\r\n" .
			  "Accept-Encoding: gzip, deflate, sdch\r\n" .
			  "Accept-Language: en-US,en;q=0.8,da;q=0.6\r\n"
  )
);
$opts = array(
  'http'=>array(
    'method'=>"GET",
    'header'=>"Accept-Language: en-US,en;q=0.8,da;q=0.6\r\n" .
              "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.116 Safari/537.36\r\n" .
			  "Accept: */*\r\n" .
			  "Cache-Control: no-cache\r\n" .
			  "Pragma: no-cache\r\n" .
			  "Connection: keep-alive\r\n" .
			  "Referer: http://www.masterani.me/"
  )
);

$context = stream_context_set_default($opts);
$context2 = stream_context_create($opts2);

if (file_exists("data/scraper.active")) die();
file_put_contents("data/scraper.active", time());
echo "scraper flag not set, continue\n";

$urusai = new Urusai();
if (!is_dir("data/urusai/")) mkdir("data/urusai/");
if (!is_dir("data/urusai/SD/")) mkdir("data/urusai/SD/");
if (!is_dir("data/urusai/HD/")) mkdir("data/urusai/HD/");

foreach ($dlQ as $anime_id => $dl) {
	if (strc($dl["base_url"], "masterani.me")) {
		for ($i = 1; $i <= 100; $i++) {
			$url = "{$dl['base_url']}{$i}";
			if ($dl["status"] == 0) {
				echo "    status = 0, breaking.\n";
				break;
			}
			$fgc = file_get_contents($url);
			echo "  parsing anime_id={$anime_id}, episode={$i}...\n";
			if (!$fgc) {
				echo "    reached 404, breaking.\n";
				break;
			}

			$dom = new DOMDocument;
			libxml_use_internal_errors(true);
			$dom->loadHTML($fgc);
			$xpath = new DOMXPath($dom);
			$nodes = $xpath->query("//div[@class='item']");
			foreach($nodes as $node) {
				$id = $node->getAttribute('data-id');
				$text = trim($node->nodeValue);
				$text = preg_replace("/[^A-Za-z0-9 ]/", '', $text);
				if (substr($text, 0, 3) === "DUB") continue;
				$hd = false;
				if (substr($text, 0, 2) === "HD") $hd = true;
				
				$host = substr($text, 2);
				$src = false;
				echo "{$id}: {$host} ({$text})<br/>\n";
				$file = "";
				if ($hd) {
					$file = strtr($dl["template"], array("%quality%" => "HD", "%episode%" => $i, "%id%" => $anime_id));
				}
				else {
					$file = strtr($dl["template"], array("%quality%" => "SD", "%episode%" => $i, "%id%" => $anime_id));
				}
				echo "  ? - {$file}\n";
				if (file_exists($file)) { echo "    exists;\n"; continue; }
				else { echo "    trying dl...\n"; }
				
				switch ($host) {
					case "Arkvid":
						$url2 = "http://www.masterani.me/anime/mirror/{$id}";
						$fgc2 = file_get_contents($url2, false, $context2);
						// iframe
						$src = substr($fgc2, strripos($fgc2, 'src="') + 5);
						$src = substr($src, 0, stripos($src, '"'));
						//echo "iframe_src={$src}<br/>\n";
						$fgc3 = file_get_contents($src);
						$src = substr($fgc3, strripos($fgc3, "<source src='") + 13);
						$src = "http:" . substr($src, 0, stripos($src, "'"));
						echo "    mp4={$src}<br/>\n";
					break;
					case "MP4Upload":
						$url2 = "http://www.masterani.me/anime/mirror/{$id}";
						$fgc2 = file_get_contents($url2, false, $context2);
						// iframe
						$src = substr($fgc2, strripos($fgc2, 'src="') + 5);
						$src = substr($src, 0, stripos($src, '"'));
						$fgc3 = file_get_contents($src);
						$src = substr($fgc3, 0, stripos($fgc3, "video.mp4") + 9);
						$src = substr($src, strripos($src, "http://"));
						echo "    mp4={$src}<br/>\n";
					break;
					case "Bakavideo":
						// not supported
						continue;
					break;
					default:
						echo "    [host not supported: {$host}]<br/>\n";
					break;
				}
				if ($src) {
					echo "    wgeting!\n";
					exec("wget \"{$src}\" -O \"{$file}\"");
					if (file_exists($file) && filesize($file) > 1024 * 1024) {
						echo "  done!\n";
						$qual = "SD";
						if ($hd) {
							$urusai->addEpisode($anime_id, $i, "HD", $file);
							$qual = "HD";
						}
						else {
							$urusai->addEpisode($anime_id, $i, "SD", $file);
						}
						$timeline = new Notify("system.timeline");
						$title = $urusai->getAnimeAttribute($anime_id, "title");
						$timeline->addNotification(array(
							"username" => "system",
							"type" => "system",
							"icon" => "cloud-upload",
							"header" => "uploaded a new episode to urusai",
							"content" => "<b>{$title}</b> &mdash; Episode {$i} in {$qual} is now available!",
							"buttons" => array(
								array(
									"href" => "?app=UrusaiPlayer&anime={$anime_id}#episode{$i}",
									"text" => "More info",
									"class" => "primary"
								)
							),
							"adminOnly" => false
						));
						echo "    (debug dir = " . __DIR__ . ")\n";
						echo "  added to timeline!\n";
						unset($timeline);
					}
					else {
						echo "  small filesize?? -- removing for now\n";
						if (file_exists($file)) unlink($file);
					}
				}
				else {
					echo "  nope, try next.\n";
				}
			}
		}
	}
	elseif (strc($dl["base_url"], "animeultima")) {
		echo "  anime: {$anime_id}, burl: {$dl['base_url']}\n";
		$site = $dl["base_url"];
		$fgc = file_get_contents($site);
		
		$dom = new DOMDocument;
		libxml_use_internal_errors(true);
		$dom->loadHTML($fgc);
		$xpath = new DOMXPath($dom);
		$nodes = $xpath->query("//td[@class='td-lang-subbed']/a");
		$episode = 1;
		foreach($nodes as $node) {
			echo "  episode: {$episode}\n";
			// check existence
			$file1 = strtr($dl["template"], array("%quality%" => "HD", "%episode%" => $episode, "%id%" => $anime_id));
			$file2 = strtr($dl["template"], array("%quality%" => "HD", "%episode%" => $episode, "%id%" => $anime_id));
			if (file_exists($file1) && file_exists($file2)) {
				echo "  episode pair for ep={$episode} exists, going to next episode.\n";
				$episode++;
				continue;
			}
			
			$src = $node->getAttribute('href');
			echo "  src={$src}\n";
			echo "  finding all mirrors...\n";
			$fgc2 = file_get_contents($src);
			$mirrors = array();
			$mirrors[] = $src;
			
			$dom2 = new DOMDocument;
			$dom2->loadHTML($fgc2);
			$xpath2 = new DOMXPath($dom2);
			$nodes2 = $xpath2->query("//div[@class='thumb']/a[@rel='nofollow']");
			foreach ($nodes2 as $node2) {
				$mirrors[] = "http://www.animeultima.io" . $node2->getAttribute("href");
			}
			
			foreach ($mirrors as $mirror) {
				echo "    mirror_src={$mirror}\n";
				if (strc($mirror, "novamov") || strc($mirror, "dailymotion") || strc($mirror, "videoweed") || strc($mirror, "videonest") || strc($mirror, "sapo") || strc($mirror, "yourupload") || strc($mirror, "youtube") || strc($mirror, "veoh")) {
					echo "      (mirror contains invalid host string, skipping)\n";
					continue;
				}
				$fgc2 = file_get_contents($mirror);
				$dom2->loadHTML($fgc2);
				$xpath2 = new DOMXPath($dom2);
				$nodes2 = $xpath2->query("//div[@id='pembed']/iframe");
				if (!$nodes->item(0)) {
					echo "      (no node available, skipping)\n";
					continue;
				}
				$mirrorurl = "";
				try {
					$mirrorurl = $nodes2->item(0)->getAttribute("src");
				}
				catch (Exception $e) {
					echo "      exception: getAttribute on non-existent node, skipping)\n";
					continue;
				}
				echo "      iframe_src={$mirrorurl}\n";
				$mp4 = false;
				if (strc($mirrorurl, "auengine.com")) {
					$fgc3 = file_get_contents($mirrorurl);
					$fgc3 = substr($fgc3, stripos($fgc3, "video_link") + 14);
					$fgc3 = substr($fgc3, 0, stripos($fgc3, "mp4") + 3);
					$mp4 = $fgc3;
				}
				elseif (strc($mirrorurl, "mp4upload.com")) {
					$fgc3 = file_get_contents($mirrorurl);
					$fgc3 = substr($fgc3, 0, stripos($fgc3, "video.mp4") + 9);
					$fgc3 = substr($fgc3, strripos($fgc3, "http://"));
					$mp4 = $fgc3;
				}
				else {
					echo "    (not supported, skipping)\n";
				}
				if ($mp4) {
					echo "      mp4={$mp4}\n";
					$head = array_change_key_case(get_headers($mp4, TRUE));
					$fs = $head["content-length"];
					$fs2 = formatBytes($fs);
					echo "      fs={$fs} ({$fs2})\n";
					$hd = false;
					if ($fs > 1024*1024*75) {
						$hd = true;
					}
					$file = "";
					if ($hd) {
						$file = strtr($dl["template"], array("%quality%" => "HD", "%episode%" => $episode, "%id%" => $anime_id));
					}
					else {
						$file = strtr($dl["template"], array("%quality%" => "SD", "%episode%" => $episode, "%id%" => $anime_id));
					}
					echo "  ? - {$file}\n";
					if (file_exists($file) && filesize($file) > 1024 * 1024) {
						echo "  file exists, continue.\n";
						continue;
					}
					echo "    wgeting!\n";
					exec("wget \"{$mp4}\" -O \"{$file}\"");
					if (file_exists($file) && filesize($file) > 1024 * 1024) {
						echo "  done!\n";
						$qual = "SD";
						if ($hd) {
							$urusai->addEpisode($anime_id, $episode, "HD", $file);
							$qual = "HD";
						}
						else {
							$urusai->addEpisode($anime_id, $episode, "SD", $file);
						}
						$timeline = new Notify("system.timeline");
						$title = $urusai->getAnimeAttribute($anime_id, "title");
						$timeline->addNotification(array(
							"username" => "system",
							"type" => "system",
							"icon" => "cloud-upload",
							"header" => "uploaded a new episode to urusai",
							"content" => "<b>{$title}</b> &mdash; Episode {$episode} in {$qual} is now available!",
							"buttons" => array(
								array(
									"href" => "?app=UrusaiPlayer&anime={$anime_id}#episode{$episode}",
									"text" => "More info",
									"class" => "primary"
								)
							),
							"adminOnly" => false
						));
						echo "    (debug dir = " . __DIR__ . ")\n";
						echo "  added to timeline!\n";
						unset($timeline);
					}
					else {
						echo "  small filesize?? -- removing for now\n";
						if (file_exists($file)) unlink($file);
					}
				}
			}
			$episode++;
		}
	}
}

unlink("data/scraper.active");
?>