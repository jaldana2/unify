<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php echo SITE_TITLE; ?></title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.5 -->
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="dist/css/AdminLTE.min.css">
	<link rel="stylesheet" href="dist/css/after.css">
	<link rel="stylesheet" href="dist/css/featherlight.min.css">
	<!-- AdminLTE Skins. We have chosen the skin-blue for this starter
		page. However, you can choose any other skin. Make sure you
		apply the skin class to the body tag so the changes take effect.
	-->
	<link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
	
	<link rel="stylesheet" href="plugins/select2/select2.min.css">
	<link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<!-- REQUIRED JS SCRIPTS -->

	<!-- jQuery 2.1.4 -->
	<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
	<!-- Bootstrap 3.3.5 -->
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<!-- AdminLTE App -->
	<script src="dist/js/app.min.js"></script>
	<!-- select2 -->
	<script src="plugins/select2/select2.min.js"></script>
	<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>

	<script src="dist/js/js.cookie.js"></script>
	<script src="dist/js/featherlight.min.js"></script>
	<script src="dist/js/socket.io.js"></script>
	<script src="dist/js/jquery.filtertable.min.js"></script>
	<script src="dist/js/jquery.tablesorter.min.js"></script>
	<script src="dist/js/jquery.autogrowtextarea.min.js"></script>
	<script src="dist/js/notify.min.js"></script>
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<?php
if (isset($_COOKIE["ui-sidebar-collapsed"]) && $_COOKIE["ui-sidebar-collapsed"] == "true") {
	echo "<body class='hold-transition skin-blue sidebar-mini sidebar-collapse'>";
}
else {
	echo "<body class='hold-transition skin-blue sidebar-mini'>";
}
?>
<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">

    <!-- Logo -->
    <a href="?app=Home" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><?php echo SITE_TITLE_MINI; ?></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><?php echo SITE_TITLE_STYLED; ?></span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button" id="navbar-resizer">
        <span class="sr-only">Toggle navigation</span>
      </a>
	  <?php
		if (isset($auth)) {
			echo "
			<!-- Navbar Right Menu -->
			  <div class='navbar-custom-menu'>
				<ul class='nav navbar-nav'>
				  <!-- Messages: style can be found in dropdown.less-->
				  <li class='dropdown messages-menu'>
					<!-- Menu toggle button -->
					<a href='#' class='dropdown-toggle' data-toggle='dropdown' id='data-mail-dd-btn'>
					  <i class='fa fa-envelope-o'></i>
					  <span class='label notify-mail-count'></span>
					</a>
					<ul class='dropdown-menu' id='data-mail-ajax'></ul>
				  </li>
				  <!-- /.messages-menu -->

				  <!-- Notifications Menu -->
				  <li class='dropdown notifications-menu'>
					<!-- Menu toggle button -->
					<a href='#' class='dropdown-toggle' data-toggle='dropdown' id='data-notification-dd-btn'>
					  <i class='fa fa-bell-o'></i>
					  <span class='label notify-count'></span>
					</a>
					<ul class='dropdown-menu' id='data-notification-ajax'></ul>
				  </li>
				  <!-- User Account Menu -->
				  <li class='dropdown user user-menu'>
					<!-- Menu Toggle Button -->
					<a href='#' class='dropdown-toggle' data-toggle='dropdown'>
					  <!-- The user image in the navbar-->
					  <div class='img-circle-div-sm x-profile-picture'></div>
					  <!-- hidden-xs hides the username on small devices so only the image appears. -->
					  <span class='hidden-xs'>" . AUTH_USER . "</span>
					</a>
					<ul class='dropdown-menu'>
					  <!-- The user image in the menu -->
					  <li class='user-header'>
						<div class='img-circle-div x-profile-picture'></div>

						<p>
						  " . AUTH_USER . "
						  <small>Welcome back!</small>
						</p>
					  </li>
					  <!-- Menu Footer-->
					  <li class='user-footer'>
						<div class='pull-left'>
						  <a href='?app=Account' class='btn btn-default btn-flat'>Profile</a>
						</div>
						<div class='pull-right'>
						  <a href='?app=process&do=logout' class='btn btn-default btn-flat'>Sign out</a>
						</div>
					  </li>
					</ul>
				  </li>
				  <!-- Control Sidebar Toggle Button -->
				  <li>
					<a href='#' data-toggle='control-sidebar'><i class='fa fa-gears'></i></a>
				  </li>
				</ul>
			  </div>
			";
		}
		else {
			echo "
			<!-- Navbar Right Menu -->
			  <div class='navbar-custom-menu'>
				<ul class='nav navbar-nav'>
				  <li>
					<a href='?app=Login&do=login'><i class='fa fa-lock'></i>&nbsp; Login / Register</a>
				  </li>
				</ul>
			  </div>
			";
		}
	  ?>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- search form (Optional) -->
      <form action="?" method="get" class="sidebar-form">
        <div class="input-group">
		  <input type="hidden" name="app" value="Search">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu">
        <li class="header">Navigation</li>
        <!-- Optionally, you can add icons to the links -->
        <li data-app="Home"><a href="?app=Home"><i class="fa fa-home"></i> <span>Home</span></a></li>
        <li data-app="Tokyo"><a href="?app=Tokyo"><i class="fa fa-music"></i> <span>t&omacr;ky&omacr; <b>music</b></span></a></li>
        <li data-app="Urusai"><a href="?app=Urusai"><i class="fa fa-video-camera"></i> <span>urusai.ninja <b>anime</b></span></a></li>
        <li data-app="Gallery"><a href="?app=Gallery"><i class="fa fa-image"></i> <span>gallery</span></a></li>
        <li data-app="Mango"><a href="?app=Mango"><i class="fa fa-lemon-o"></i> <span>mango <b>manga reader</b></span></a></li>
        <li data-app="Status"><a href="?app=StatusCake"><i class="fa fa-hourglass-half"></i> <span>statuscake <b>uptime stats</b></span></a></li>
		<!--
		<li class="treeview">
		  <a href="#"><i class="fa fa-link"></i> <span>More</span> <i class="fa fa-angle-left pull-right"></i></a>
		  <ul class="treeview-menu">
			<li><a href="#">...</a></li>
		  </ul>
		</li>
		-->
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	
	<?php if (file_exists("header/{$app}.php")) include "header/{$app}.php"; ?>
	
    </section>

    <!-- Main content -->
    <section class="content">

	<?php include "bin/{$app}.php"; ?>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
	  &mdash; @bitbucket <a href="https://bitbucket.org/jaldana2/unify">&quot;unify&quot;</a>
    </div>
    <!-- Default to the left -->
    <strong>2016 <a href="?app=Home">after|mirror</a></strong>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane active" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Theme Settings</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="#" set-theme-data="blue" class="set-theme-btn">
              <i class="menu-icon bg-blue"></i>
              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Blue</h4>
                <p>Default Theme (Site default)</p>
              </div>
            </a>
          </li>
          <li>
            <a href="#" set-theme-data="blue-light" class="set-theme-btn">
              <i class="menu-icon fa fa-circle bg-blue"></i>
              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Blue</h4>
                <p>Light Theme</p>
              </div>
            </a>
          </li>
          <li>
            <a href="#" set-theme-data="black" class="set-theme-btn">
              <i class="menu-icon bg-black"></i>
              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Black</h4>
                <p>Default Theme</p>
              </div>
            </a>
          </li>
          <li>
            <a href="#" set-theme-data="black-light" class="set-theme-btn">
              <i class="menu-icon fa fa-circle bg-black"></i>
              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Black</h4>
                <p>Light Theme</p>
              </div>
            </a>
          </li>
          <li>
            <a href="#" set-theme-data="purple" class="set-theme-btn">
              <i class="menu-icon bg-purple"></i>
              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Purple</h4>
                <p>Default Theme</p>
              </div>
            </a>
          </li>
          <li>
            <a href="#" set-theme-data="purple-light" class="set-theme-btn">
              <i class="menu-icon fa fa-circle bg-purple"></i>
              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Purple</h4>
                <p>Light Theme</p>
              </div>
            </a>
          </li>
          <li>
            <a href="#" set-theme-data="green" class="set-theme-btn">
              <i class="menu-icon bg-green"></i>
              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Green</h4>
                <p>Default Theme</p>
              </div>
            </a>
          </li>
          <li>
            <a href="#" set-theme-data="green-light" class="set-theme-btn">
              <i class="menu-icon fa fa-circle bg-green"></i>
              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Green</h4>
                <p>Light Theme</p>
              </div>
            </a>
          </li>
          <li>
            <a href="#" set-theme-data="red" class="set-theme-btn">
              <i class="menu-icon bg-red"></i>
              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Red</h4>
                <p>Default Theme</p>
              </div>
            </a>
          </li>
          <li>
            <a href="#" set-theme-data="red-light" class="set-theme-btn">
              <i class="menu-icon fa fa-circle bg-red"></i>
              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Red</h4>
                <p>Light Theme</p>
              </div>
            </a>
          </li>
          <li>
            <a href="#" set-theme-data="yellow" class="set-theme-btn">
              <i class="menu-icon bg-yellow"></i>
              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Yellow</h4>
                <p>Default Theme</p>
              </div>
            </a>
          </li>
          <li>
            <a href="#" set-theme-data="yellow-light" class="set-theme-btn">
              <i class="menu-icon fa fa-circle bg-yellow"></i>
              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Yellow</h4>
                <p>Light Theme</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">More Settings</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Clickable checkbox
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              One of these days this button will do something!
            </p>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. Slimscroll is required when using the
     fixed layout. -->
<script>
	$(function() {
		$("#navbar-resizer").click(function() {
			if ($("body").hasClass("sidebar-collapse")) { // event fires before class switch, flip
				Cookies.set("ui-sidebar-collapsed", false, { expires: 30 });
			}
			else {
				Cookies.set("ui-sidebar-collapsed", true, { expires: 30 });
			}
		});
		// pAjax
		$("#data-mail-dd-btn").click(function() {
			$("#data-mail-ajax").load("?app=pAjax&do=Messages");
		});
		$("#data-notification-dd-btn").click(function() {
			$("#data-notification-ajax").load("?app=pAjax&do=Notifications");
		});
		$('.x-profile-picture').css('background-image', 'url(?app=img&do=profile_picture&v=<?php echo AUTH_USER; ?>)');
		$(".set-theme-btn").click(function() {
			Cookies.set("ui-theme", $(this).attr("set-theme-data"));
			resetUITheme($(this).attr("set-theme-data"));
			$.notify('Theme changed!', { position: 'bottom right', className: 'info' });
		});
		
		function resetUITheme(ui_theme) {
			$("body").removeClass("skin-blue skin-black skin-red skin-yellow skin-purple skin-green skin-blue-light skin-black-light skin-red-light skin-yellow-light skin-purple-light skin-green-light").addClass("skin-" + ui_theme);
		}
		var ui_theme = Cookies.get("ui-theme");
		if (ui_theme != undefined) resetUITheme(ui_theme);
		
		// notifier
		var prev_notification_count = 0;
		var prev_notification_mail_count = 0;
		function updateNotifications() {
			$.get('?app=notifications-ajax', function(data) {
				e = data.substring(0, data.indexOf(','));
				f = data.substring(data.indexOf(',') + 1);
				if (e !== prev_notification_count) {
					if (e !== '0' && e !== '') {
						$.notify('New notification!', { position: 'bottom right', className: 'info' });
						$(".notify-count").show().addClass("label-info");
					}
					else {
						$(".notify-count").hide().removeClass("label-info");
					}
					prev_notification_count = e;
					$('.notify-count').text(e);
				}
				if (f !== prev_notification_mail_count) {
					if (f !== '0' && f !== '') {
						$.notify('New message!', { position: 'bottom right', className: 'info' });
						$(".notify-mail-count").show().addClass("label-info");
					}
					else {
						$(".notify-mail-count").hide().removeClass("label-info");
					}
					prev_notification_mail_count = f;
					$('.notify-mail-count').text(f);
				}
			});
		}
		updateNotifications();
		setInterval(updateNotifications, 15000);
		$('body').on('click', '.notify-del-btn', function(e) {
			var nID = $(this).attr('notifyID');
			$.get('?app=notifications-ajax&del=' + nID);
			$('#notify-' + nID).fadeOut(200);
		});
		<?php
			if (defined("PARENT_APP")) {
				echo "$('li[data-app=" . PARENT_APP . "]').addClass('active');";
			}
			else {
				echo "$('li[data-app=Home]').addClass('active');";
			}
		?>
	});
</script>
</body>
</html>
