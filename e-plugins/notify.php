<?php
class Notify {
	var $username;
	var $db;
	
	function __construct($username) {
		$this->username = $username;
		$this->loadDB();
	}
	function loadDB() {
		$this->db = readDB("data/account/notify/" . $this->username . ".notify");
	}
	function saveDB() {
		writeDB("data/account/notify/" . $this->username . ".notify", $this->db);
	}
	function getNotifications() {
		return $this->db;
	}
	function addNotification($data) {
		$data["_read"] = false;
		$data["_sent"] = time();
		$this->db[] = $data;
		$this->saveDB();
	}
	function removeNotification($notifyID) {
		$this->db = pushValueFromArray((int) $notifyID, $this->db);
		$this->saveDB();
	}
	
	function countUnreadNotifications() {
		$count = 0;
		foreach ($this->db as $data) {
			if (!$data["_read"]) $count++;
		}
		return $count;
	}
	function readNotification($notifyID) {
		$this->db[$notifyID]["_read"] = true;
		$this->saveDB();
	}
	function getNotification($notifyID) {
		return $this->db[$notifyID];
	}
	function setNotification($notifyID, $data) {
		$this->db[$notifyID] = $data;
		$this->saveDB();
	}
}
?>