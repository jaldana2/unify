<?php
define("AUTH_ACCESS_GUEST", 0);
define("AUTH_ACCESS_NORMAL", 1);
define("AUTH_ACCESS_VERIFIED", 2);
define("AUTH_ACCESS_BANNED", 10);
define("AUTH_ACCESS_ADMIN", 29);

define("AUTH_ERROR_USER_EXISTS", 100);
define("AUTH_ERROR_NO_ERROR", 190);
define("AUTH_ERROR_GENERIC", 199);
define("AUTH_ERROR_CRYPT_INVALID_ERROR", 120);

define("AUTH_SALT", sha1("pepppper"));

class Auth {
	var $db;
	
	function __construct() {
		$this->db = new SQLite3("data/system.db");
		$this->init();
	}
	
	function init() {
		$query = "
		CREATE TABLE IF NOT EXISTS users (
			uid INTEGER PRIMARY KEY AUTOINCREMENT,
			username STRING,
			password STRING,
			session STRING,
			access INTEGER
		);
		CREATE TABLE IF NOT EXISTS profile (
			uid STRING PRIMARY KEY,
			points INTEGER
		);
		CREATE TABLE IF NOT EXISTS access (
			eid INTEGER PRIMARY KEY AUTOINCREMENT,
			ip STRING,
			username STRING,
			url STRING,
			action STRING,
			ts INTEGER
		);
		";
		$this->db->exec($query);
	}
	
	function getPoints($username) {
		$uid = $this->getUserID($username);
		return $this->getProfileAttribute($uid, "points");
	}
	function modPoints($username, $value) {
		$uid = $this->getUserID($username);
		$points = $this->getPoints($username);
		if (!$points) {
			$points = 0;
		}
		$points += $value;
		$this->setProfileAttribute($uid, "points", $points, SQLITE3_INTEGER);
	}
	
	
	function getProfileAttribute($uid, $attribute) {
		$stmt = $this->db->prepare("SELECT * FROM profile WHERE uid = :uid;");
		$stmt->bindValue('uid', $uid, SQLITE3_INTEGER);
		
		$res = $stmt->execute();
		
		$row = $res->fetchArray();
		if ($row !== false) {
			// exists
			return $row[$attribute];
		}
		else {
			// doesn't exist
			return false;
		}
	}
	function setProfileAttribute($uid, $attribute, $value, $type) {
		$stmt = $this->db->prepare("UPDATE profile SET " . $attribute . " = :value WHERE uid = :uid;");
		$stmt->bindValue('uid', $uid, SQLITE3_INTEGER);
		$stmt->bindValue('value', $value, $type);
		
		$res = $stmt->execute();
	}
	
	function getUserAttribute($username, $attribute) {
		$stmt = $this->db->prepare("SELECT * FROM users WHERE username = :username;");
		$stmt->bindValue('username', $username, SQLITE3_TEXT);
		
		$res = $stmt->execute();
		
		$row = $res->fetchArray();
		if ($row !== false) {
			// exists
			return $row[$attribute];
		}
		else {
			// doesn't exist
			return false;
		}
	}
	function getUserID($username) {
		return $this->getUserAttribute($username, "uid");
	}
	function getUserSessionKey($username) {
		return $this->getUserAttribute($username, "session");
	}
	function getUserPassword($username) {
		return $this->getUserAttribute($username, "password");
	}
	function getUserAccess($username) {
		return $this->getUserAttribute($username, "access");
	}
	
	function getNewSessionKey($username) {
		return substr(sha1($username), 5, 5).':'.sha1(uniqid().$username);
	}
	function getNewPasswordHash($password) {
		return sha1($password.AUTH_SALT);
	}
	
	function setUserAttribute($username, $attribute, $value, $type) {
		$stmt = $this->db->prepare("UPDATE users SET " . $attribute . " = :value WHERE username = :username;");
		$stmt->bindValue('username', $username, SQLITE3_TEXT);
		$stmt->bindValue('value', $value, $type);
		
		$res = $stmt->execute();
	}
	function setUserSessionKey($username, $key) {
		$this->setUserAttribute($username, "session", $key, SQLITE3_TEXT);
	}
	function setUserPassword($username, $key) {
		$this->setUserAttribute($username, "password", $this->getNewPasswordHash($key), SQLITE3_TEXT);
	}
	function setUserAccess($username, $key) {
		$this->setUserAttribute($username, "access", $key, SQLITE3_INTEGER);
	}
	
	function checkUserExists($username) {
		return $this->getUserID($username) ? true : false;
	}
	function checkIfPasswordless($username) {
		return ($this->getUserPassword($username) == $this->getNewPasswordHash('')) ? true : false;
	}
	function checkUserSession($session) {
		$stmt = $this->db->prepare("SELECT * FROM users WHERE session = :session;");
		$stmt->bindValue('session', $session, SQLITE3_TEXT);
		
		$res = $stmt->execute();
		
		$row = $res->fetchArray();
		if ($row !== false) {
			// exists
			return $row["username"];
		}
		else {
			// doesn't exist
			return false;
		}
	}
	function checkIfProfilePictureExists($username) {
		return file_exists("data/account/profile/{$username}.jpg");
	}
	
	function registerNewUser($username, $password) {
		if ($this->checkUserExists($username)) {
			return AUTH_ERROR_USER_EXISTS;
		}
		$stmt = $this->db->prepare("INSERT INTO users (username, password, session, access) VALUES (:username, :password, :session, :access);");
		$stmt->bindValue('username', $username, SQLITE3_TEXT);
		if (strlen($password) > 0) {
			$stmt->bindValue('password', $this->getNewPasswordHash($password), SQLITE3_TEXT);
		}
		else {				
			$stmt->bindValue('password', $this->getNewPasswordHash(''), SQLITE3_TEXT);
		}
		$stmt->bindValue('session', $this->getNewSessionKey($username), SQLITE3_TEXT);
		$stmt->bindValue('access', AUTH_ACCESS_NORMAL, SQLITE3_INTEGER);
		
		$stmt->execute();
		
		$uid = $this->getUserID($username);
		
		unset($stmt);
		$stmt = $this->db->prepare("INSERT INTO profile (uid, points) VALUES (:uid, 0);");
		$stmt->bindValue('uid', $uid, SQLITE3_INTEGER);
		$stmt->execute();
		
		return AUTH_ERROR_NO_ERROR;
	}
	
	function getAllUsers() {
		$stmt = $this->db->prepare("SELECT * FROM users;");
		
		$res = $stmt->execute();
		
		$return = array();
		$row = $res->fetchArray();
		if ($row !== false) {
			// exists
			do {
				$return[] = $row["username"];
			} while ($row = $res->fetchArray());
		}
		return $return;
	}
}

// DEFINE our cipher
define('AES_256_CBC', 'aes-256-cbc');
define('AUTH_SYSTEM_KEY', file_get_contents("data/system.key"));

function fnEncrypt($data) {
	$iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length(AES_256_CBC));
	$encrypted = openssl_encrypt($data, AES_256_CBC, AUTH_SYSTEM_KEY, 0, $iv);
	return $encrypted . ':' . base64_encode($iv);
}
function fnDecrypt($data) {
	$parts = explode(':', $data);
	return openssl_decrypt($parts[0], AES_256_CBC, AUTH_SYSTEM_KEY, 0, base64_decode($parts[1]));
}

// --

// auto login
if (isset($_COOKIE["auth"])) {
	if (!isset($auth)) $auth = new Auth();
	$_session = $auth->checkUserSession($_COOKIE["auth"]);
	if ($_session) {
		define("AUTH_USER", $_session);
	}
}

function requireLogin() {
	if (!defined("AUTH_USER")) {
		$qs = base64_encode($_SERVER["QUERY_STRING"]);
		if (!headers_sent()) {
			header("Location: ?app=Login&do=login&return={$qs}");
		}
		else {
			echo "
			<script>location.href = '?app=Login&do=login&return={$qs}';</script>
			<h1>Login Required.</h1>
			<a href='?app=Login&do=login&return={$qs}'>Click me to go to the login page.</a>
			";
			die();
		}
	}
}
function enforceLogin() {
	if (!defined("AUTH_USER")) {
		die();
	}
}
?>