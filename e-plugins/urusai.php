<?php
define("URUSAI_ANIME_ERROR", -1);
class Urusai {
	var $db;
	
	function __construct() {
		$this->db = new SQLite3("data/urusai3.db");
		$this->init();
	}
	function init() {
		$query = "
		CREATE TABLE IF NOT EXISTS anime (
			id INTEGER PRIMARY KEY AUTOINCREMENT,
			title STRING,
			alt_title STRING,
			air_season STRING,
			num_episodes INTEGER,
			genres STRING,
			rating STRING,
			synopsis TEXT,
			airing INTEGER,
			status INTEGER
		);
		CREATE TABLE IF NOT EXISTS episodes (
			id INTEGER PRIMARY KEY AUTOINCREMENT,
			anime_id INTEGER,
			episode INTEGER,
			quality STRING,
			src STRING
		);
		";
		$this->db->exec($query);
	}
	
	function getListing() {
		$stmt = $this->db->prepare("SELECT * FROM anime;");
		
		$res = $stmt->execute();
		
		$return = array();
		$row = $res->fetchArray();
		if ($row !== false) {
			// exists
			do {
				$return[$row["id"]] = $row["title"];
			} while ($row = $res->fetchArray());
		}
		return $return;
	}
	function animeExists($title) {
		$stmt = $this->db->prepare("SELECT * FROM anime WHERE title = :title;");
		$stmt->bindValue('title', $title, SQLITE3_TEXT);
		
		$res = $stmt->execute();
		
		$row = $res->fetchArray();
		if ($row !== false) {
			// exists
			return true;
		}
		else {
			// doesn't exist
			return false;
		}
	}
	function addAnime($title, $alt_title, $air_season, $num_episodes, $genres, $rating, $synopsis, $airing, $status) {
		$stmt = $this->db->prepare("INSERT INTO anime (title, alt_title, air_season, num_episodes, genres, rating, synopsis, airing, status) VALUES (:title, :alt_title, :air_season, :num_episodes, :genres, :rating, :synopsis, :airing, :status);");
		$stmt->bindValue('title', $title, SQLITE3_TEXT);
		$stmt->bindValue('alt_title', $alt_title, SQLITE3_TEXT);
		$stmt->bindValue('air_season', $air_season, SQLITE3_TEXT);
		$stmt->bindValue('num_episodes', $num_episodes, SQLITE3_INTEGER);
		$stmt->bindValue('genres', $genres, SQLITE3_TEXT);
		$stmt->bindValue('rating', $rating, SQLITE3_TEXT);
		$stmt->bindValue('synopsis', $synopsis, SQLITE3_TEXT);
		$stmt->bindValue('airing', $airing, SQLITE3_INTEGER);
		$stmt->bindValue('status', $status, SQLITE3_INTEGER);
		
		$stmt->execute();
	}
	function getAnimeAttribute($id, $attribute) {
		$stmt = $this->db->prepare("SELECT * FROM anime WHERE id = :id;");
		$stmt->bindValue('id', $id, SQLITE3_INTEGER);
		
		$res = $stmt->execute();
		
		$row = $res->fetchArray();
		if ($row !== false) {
			// exists
			return $row[$attribute];
		}
		else {
			// doesn't exist
			return false;
		}
	}
	function setAnimeAttribute($id, $attribute, $value, $type) {
		$stmt = $this->db->prepare("UPDATE anime SET " . $attribute . " = :value WHERE id = :id;");
		$stmt->bindValue('id', $id, SQLITE3_INTEGER);
		$stmt->bindValue('value', $value, $type);
		
		$res = $stmt->execute();
	}
	function getAnimeAttributes($id) {
		$stmt = $this->db->prepare("SELECT * FROM anime WHERE id = :id;");
		$stmt->bindValue('id', $id, SQLITE3_INTEGER);
		
		$res = $stmt->execute();
		
		$row = $res->fetchArray();
		if ($row !== false) {
			// exists
			return $row;
		}
		return false;
	}
	
	function getEpisodeMirrors($anime_id, $episode) {
		$stmt = $this->db->prepare("SELECT * FROM episodes WHERE anime_id = :anime_id AND episode = :episode;");
		$stmt->bindValue('anime_id', $anime_id, SQLITE3_INTEGER);
		$stmt->bindValue('episode', $episode, SQLITE3_INTEGER);
		
		$res = $stmt->execute();
		
		$return = array();
		$row = $res->fetchArray();
		if ($row !== false) {
			// exists
			do {
				$return[$row["id"]] = array(
					"episode" => $row["episode"],
					"quality" => $row["quality"],
					"src" => $row["src"]
				);
			} while ($row = $res->fetchArray());
		}
		return $return;
	}
	function getEpisodes($id) {
		$stmt = $this->db->prepare("SELECT * FROM episodes WHERE anime_id = :id;");
		$stmt->bindValue('id', $id, SQLITE3_INTEGER);
		
		$res = $stmt->execute();
		
		$return = array();
		$row = $res->fetchArray();
		if ($row !== false) {
			// exists
			do {
				$return[$row["id"]] = array(
					"episode" => $row["episode"],
					"quality" => $row["quality"],
					"src" => $row["src"]
				);
			} while ($row = $res->fetchArray());
		}
		return $return;
	}
	function getEpisode($id) {
		$stmt = $this->db->prepare("SELECT * FROM episodes WHERE id = :id;");
		$stmt->bindValue('id', $id, SQLITE3_INTEGER);
		
		$res = $stmt->execute();
		
		$row = $res->fetchArray();
		if ($row !== false) {
			// exists
			do {
				return array(
					"anime_id" => $row["anime_id"],
					"episode" => $row["episode"],
					"quality" => $row["quality"],
					"src" => $row["src"]
				);
			} while ($row = $res->fetchArray());
		}
		return false;
	}
	function addEpisode($id, $episode, $quality, $src) {
		$stmt = $this->db->prepare("INSERT INTO episodes (anime_id, episode, quality, src) VALUES (:anime_id, :episode, :quality, :src);");
		$stmt->bindValue('anime_id', $id, SQLITE3_INTEGER);
		$stmt->bindValue('episode', $episode, SQLITE3_INTEGER);
		$stmt->bindValue('quality', $quality, SQLITE3_TEXT);
		$stmt->bindValue('src', $src, SQLITE3_TEXT);
		
		$stmt->execute();
	}
	function setEpisodeAttribute($id, $attribute, $value, $type) {
		$stmt = $this->db->prepare("UPDATE episodes SET " . $attribute . " = :value WHERE id = :id;");
		$stmt->bindValue('id', $id, SQLITE3_INTEGER);
		$stmt->bindValue('value', $value, $type);
		
		$res = $stmt->execute();
	}
	function removeEpisode($id) {
		$stmt = $this->db->prepare("DELETE FROM episodes WHERE id = :id;");
		$stmt->bindValue('id', $id, SQLITE3_INTEGER);
		
		$res = $stmt->execute();
	}
	function purgeEpisodes($anime_id) {
		$stmt = $this->db->prepare("DELETE FROM episodes WHERE anime_id = :anime_id;");
		$stmt->bindValue('anime_id', $anime_id, SQLITE3_INTEGER);
		
		$res = $stmt->execute();
	}
}
?>