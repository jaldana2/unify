<?php
class Gallery {
	var $db;
	
	function __construct() {
		$this->db = new SQLite3("data/gallery.db");
		$this->init();
	}
	function init() {
		$query = "
		CREATE TABLE IF NOT EXISTS collections (
			id INTEGER PRIMARY KEY AUTOINCREMENT,
			title STRING,
			description STRING,
			creator STRING,
			password STRING,
			public INTEGER,
			visible INTEGER
		);
		CREATE TABLE IF NOT EXISTS media (
			id INTEGER PRIMARY KEY AUTOINCREMENT,
			collection_id INTEGER,
			title STRING,
			src STRING,
			tags STRING,
			uploader STRING,
			time INTEGER
		);
		";
		$this->db->exec($query);
	}
	
	function getCollections() {
		$stmt = $this->db->prepare("SELECT * FROM collections;");
		
		$res = $stmt->execute();
		
		$return = array();
		$row = $res->fetchArray();
		if ($row !== false) {
			// exists
			do {
				$return[$row["id"]] = $row["title"];
			} while ($row = $res->fetchArray());
		}
		return $return;
	}
	function createCollection($title, $description, $creator) {
		$stmt = $this->db->prepare("INSERT INTO collections (title, description, creator, password, public, visible) VALUES (:title, :description, :creator, :password, :public, :visible);");
		$stmt->bindValue('title', $title, SQLITE3_TEXT);
		$stmt->bindValue('description', $description, SQLITE3_TEXT);
		$stmt->bindValue('creator', $creator, SQLITE3_TEXT);
		$stmt->bindValue('password', "", SQLITE3_TEXT);
		$stmt->bindValue('public', 1, SQLITE3_INTEGER);
		$stmt->bindValue('visible', 1, SQLITE3_INTEGER);
		
		$stmt->execute();
	}
	function setCollectionAttribute($id, $attribute, $value, $type) {
		$stmt = $this->db->prepare("UPDATE collections SET " . $attribute . " = :value WHERE id = :id;");
		$stmt->bindValue('id', $id, SQLITE3_INTEGER);
		$stmt->bindValue('value', $value, $type);
		
		$res = $stmt->execute();
	}
	function getCollectionAttributes($id) {
		$stmt = $this->db->prepare("SELECT * FROM collections WHERE id = :id;");
		$stmt->bindValue('id', $id, SQLITE3_INTEGER);
		
		$res = $stmt->execute();
		
		$row = $res->fetchArray();
		if ($row !== false) {
			// exists
			return $row;
		}
		return false;
	}
	
	function getCollectionMedia($collection_id) {
		$stmt = $this->db->prepare("SELECT * FROM media WHERE collection_id = :id;");
		$stmt->bindValue('id', $collection_id, SQLITE3_INTEGER);
		
		$res = $stmt->execute();
		
		$return = array();
		$row = $res->fetchArray();
		if ($row !== false) {
			// exists
			do {
				$return[$row["id"]] = $row;
			} while ($row = $res->fetchArray());
		}
		return $return;
	}
	function getMediaAttributes($id) {
		$stmt = $this->db->prepare("SELECT * FROM media WHERE id = :id;");
		$stmt->bindValue('id', $id, SQLITE3_INTEGER);
		
		$res = $stmt->execute();
		
		$row = $res->fetchArray();
		if ($row !== false) {
			// exists
			return $row;
		}
		return false;
	}
	function addMedia($collection_id, $file, $title, $tags) {
		$stmt = $this->db->prepare("INSERT INTO media (collection_id, title, src, tags, uploader, time) VALUES (:collection_id, :title, :src, :tags, :uploader, :time);");
		$stmt->bindValue('collection_id', $collection_id, SQLITE3_INTEGER);
		$stmt->bindValue('title', $title, SQLITE3_TEXT);
		$stmt->bindValue('src', $file, SQLITE3_TEXT);
		$stmt->bindValue('tags', $tags, SQLITE3_TEXT);
		$stmt->bindValue('uploader', AUTH_USER, SQLITE3_TEXT);
		$stmt->bindValue('time', time(), SQLITE3_INTEGER);
		
		$stmt->execute();
	}
	function setMediaAttribute($id, $attribute, $value, $type) {
		$stmt = $this->db->prepare("UPDATE media SET " . $attribute . " = :value WHERE id = :id;");
		$stmt->bindValue('id', $id, SQLITE3_INTEGER);
		$stmt->bindValue('value', $value, $type);
		
		$res = $stmt->execute();
	}
}
?>