<?php
//enforceLogin();

function curl_get_file_size( $url ) {
  // Assume failure.
  $result = -1;

  $curl = curl_init( $url );

  // Issue a HEAD request and follow any redirects.
  curl_setopt( $curl, CURLOPT_NOBODY, true );
  curl_setopt( $curl, CURLOPT_HEADER, true );
  curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
  curl_setopt( $curl, CURLOPT_FOLLOWLOCATION, true );

  $data = curl_exec( $curl );
  curl_close( $curl );

  if( $data ) {
    $content_length = "unknown";
    $status = "unknown";

    if( preg_match( "/^HTTP\/1\.[01] (\d\d\d)/", $data, $matches ) ) {
      $status = (int)$matches[1];
    }

    if( preg_match( "/Content-Length: (\d+)/", $data, $matches ) ) {
      $content_length = (int)$matches[1];
    }

    // http://en.wikipedia.org/wiki/List_of_HTTP_status_codes
    if( $status == 200 || ($status > 300 && $status <= 308) ) {
      $result = $content_length;
    }
  }

  return $result;
}

if (isset($_GET["gfs"])) {
	echo curl_get_file_size($_GET["gfs"]);
}
else {
	$urusai = new Urusai();

	$listing = $urusai->getListing();
	echo "
	<div style='color: black;'>
	";
	foreach ($listing as $id => $title) {
		echo "<h4>{$title}</h4>";
		$episodes = $urusai->getEpisodes($id);
		foreach ($episodes as $episode => $data) {
			$e_low = $data["src_low"];
			$e_high = $data["src_high"];
			$episode_num = $episode;
			if ($episode_num < 10) { $episode_num = "0" . $episode_num; }
			$e_low = strtr($e_low, array("%ep_num%" => $episode_num));
			$e_high = strtr($e_high, array("%ep_num%" => $episode_num));
			if (strlen($e_low.$e_high) > 0) {
				if (isSomething($e_low)) {
					echo "<span class='btn btn-xs btn-primary ajax-load-btn' media-src='{$e_low}'>Watch</span> ";
				}
				if (isSomething($e_high)) {
					echo "<span class='btn btn-xs btn-primary ajax-load-btn' media-src='{$e_high}'>Watch (HD)</span> ";
				}
			}
			else {
				echo "<span class='btn btn-xs btn-warning'>(not available)</span> ";
			}
			echo "<b>Episode {$episode}</b>";
			if (strlen($data["title"]) > 0) {
				echo ": {$data['title']}";
			}
			echo "<br/>";
		}
	}
	echo "
	</div>
	<div style='display: none;'>
		<script>
			$('.ajax-load-btn').off('click').on('click', function(e) {
				url = $(this).attr('media-src');
				socket.emit('interact', { action: 'url', value: url, room: room });
				var bn = url.substring(url.lastIndexOf('/') + 1);
				bn = bn.replace(/%20/g, ' ').replace(/\\[.*?\\]/g, '');
				bn = bn.substring(0, bn.lastIndexOf('.'));
				socket.emit('chat message', { message: name + ' loaded a new video: ' + bn, username: '[S]', room: room, skipfilter: true });
				
				var current = $.featherlight.current();
				current.close();
			});
		</script>
	</div>
	";
}

?>