<?php
requireLogin();

if (isset($_GET["do"])) {
	switch ($_GET["do"]) {
		case "profile-image-upload":
			if (!empty($_FILES)) {
				$tmp_name = $_FILES["file"]["tmp_name"];
				$file = AUTH_USER . ".jpg";
				$target = "data/account/profile/{$file}";
				if (file_exists($target)) {
					unlink($target);
					$auth->modPoints(AUTH_USER, 200);
					echo "
					<script>
						$.notify('You earned 200 ¥ for uploading a new profile picture.', { className: 'info', position: 'bottom right' });
					</script>
					";
				}
				else {
					$auth->modPoints(AUTH_USER, 2000);
					echo "
					<script>
						$.notify('You earned 2000 ¥ for uploading your first profile picture.', { className: 'info', position: 'bottom right' });
					</script>
					";
				}
				iTF($tmp_name, $target, 640, 92);
			}
		break;
	}
	echo "
	<div class='container'>
		<p>Please wait... <span class='fa fa-spinner fa-spin'></span></p>
		<br />
		<a href='?app=Account' class='btn btn-info'>Click me if you are not redirected within a few seconds</a>
	</div>
	<script>
		function refresher() {
			window.location = '?app=Account&refresh';	
		}
		setTimeout('refresher()', 2500);
	</script>
	";
}
else {
	echo "
	<div class='profile-header'>
		<div class='profile-picture-lg x-profile-picture'>
			<span class='profile-picture-edit-btn btn btn-xs btn-primary' id='profile-image-upload-btn'><span class='fa fa-pencil'></span></span>
		</div>
		<h1 class='profile-title'>" . AUTH_USER . "</h1>
	</div>
	<div style='display: none;'>
		<form action='?app=Account&do=profile-image-upload' method='post' enctype='multipart/form-data' id='file-upl'>
			<input type='file' name='file' id='file'>
		</form>
	</div>
	<script>
		$('#file').change(function() {
			$('#file-upl').submit();
		});
		$('#profile-image-upload-btn').on('click', function() {
			$('#file').click();
		});
	</script>
	";	
}
?>