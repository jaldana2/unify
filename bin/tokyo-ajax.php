<?php
class MP3File
{
    protected $filename;
    public function __construct($filename)
    {
        $this->filename = $filename;
    }
 
    public static function formatTime($duration) //as hh:mm:ss
    {
        //return sprintf("%d:%02d", $duration/60, $duration%60);
        $hours = floor($duration / 3600);
        $minutes = floor( ($duration - ($hours * 3600)) / 60);
        $seconds = $duration - ($hours * 3600) - ($minutes * 60);
        return sprintf("%02d:%02d:%02d", $hours, $minutes, $seconds);
    }
 
    //Read first mp3 frame only...  use for CBR constant bit rate MP3s
    public function getDurationEstimate()
    {
        return $this->getDuration($use_cbr_estimate=true);
    }
 
    //Read entire file, frame by frame... ie: Variable Bit Rate (VBR)
    public function getDuration($use_cbr_estimate=false)
    {
        $fd = fopen($this->filename, "rb");
 
        $duration=0;
        $block = fread($fd, 100);
        $offset = $this->skipID3v2Tag($block);
        fseek($fd, $offset, SEEK_SET);
        while (!feof($fd))
        {
            $block = fread($fd, 10);
            if (strlen($block)<10) { break; }
            //looking for 1111 1111 111 (frame synchronization bits)
            else if ($block[0]=="\xff" && (ord($block[1])&0xe0) )
            {
                $info = self::parseFrameHeader(substr($block, 0, 4));
                if (empty($info['Framesize'])) { return $duration; } //some corrupt mp3 files
                fseek($fd, $info['Framesize']-10, SEEK_CUR);
                $duration += ( $info['Samples'] / $info['Sampling Rate'] );
            }
            else if (substr($block, 0, 3)=='TAG')
            {
                fseek($fd, 128-10, SEEK_CUR);//skip over id3v1 tag size
            }
            else
            {
                fseek($fd, -9, SEEK_CUR);
            }
            if ($use_cbr_estimate && !empty($info))
            { 
                return $this->estimateDuration($info['Bitrate'],$offset); 
            }
        }
        return round($duration);
    }
 
    private function estimateDuration($bitrate,$offset)
    {
        $kbps = ($bitrate*1000)/8;
        $datasize = filesize($this->filename) - $offset;
        return round($datasize / $kbps);
    }
 
    private function skipID3v2Tag(&$block)
    {
        if (substr($block, 0,3)=="ID3")
        {
            $id3v2_major_version = ord($block[3]);
            $id3v2_minor_version = ord($block[4]);
            $id3v2_flags = ord($block[5]);
            $flag_unsynchronisation  = $id3v2_flags & 0x80 ? 1 : 0;
            $flag_extended_header    = $id3v2_flags & 0x40 ? 1 : 0;
            $flag_experimental_ind   = $id3v2_flags & 0x20 ? 1 : 0;
            $flag_footer_present     = $id3v2_flags & 0x10 ? 1 : 0;
            $z0 = ord($block[6]);
            $z1 = ord($block[7]);
            $z2 = ord($block[8]);
            $z3 = ord($block[9]);
            if ( (($z0&0x80)==0) && (($z1&0x80)==0) && (($z2&0x80)==0) && (($z3&0x80)==0) )
            {
                $header_size = 10;
                $tag_size = (($z0&0x7f) * 2097152) + (($z1&0x7f) * 16384) + (($z2&0x7f) * 128) + ($z3&0x7f);
                $footer_size = $flag_footer_present ? 10 : 0;
                return $header_size + $tag_size + $footer_size;//bytes to skip
            }
        }
        return 0;
    }
 
    public static function parseFrameHeader($fourbytes)
    {
        static $versions = array(
            0x0=>'2.5',0x1=>'x',0x2=>'2',0x3=>'1', // x=>'reserved'
        );
        static $layers = array(
            0x0=>'x',0x1=>'3',0x2=>'2',0x3=>'1', // x=>'reserved'
        );
        static $bitrates = array(
            'V1L1'=>array(0,32,64,96,128,160,192,224,256,288,320,352,384,416,448),
            'V1L2'=>array(0,32,48,56, 64, 80, 96,112,128,160,192,224,256,320,384),
            'V1L3'=>array(0,32,40,48, 56, 64, 80, 96,112,128,160,192,224,256,320),
            'V2L1'=>array(0,32,48,56, 64, 80, 96,112,128,144,160,176,192,224,256),
            'V2L2'=>array(0, 8,16,24, 32, 40, 48, 56, 64, 80, 96,112,128,144,160),
            'V2L3'=>array(0, 8,16,24, 32, 40, 48, 56, 64, 80, 96,112,128,144,160),
        );
        static $sample_rates = array(
            '1'   => array(44100,48000,32000),
            '2'   => array(22050,24000,16000),
            '2.5' => array(11025,12000, 8000),
        );
        static $samples = array(
            1 => array( 1 => 384, 2 =>1152, 3 =>1152, ), //MPEGv1,     Layers 1,2,3
            2 => array( 1 => 384, 2 =>1152, 3 => 576, ), //MPEGv2/2.5, Layers 1,2,3
        );
        //$b0=ord($fourbytes[0]);//will always be 0xff
        $b1=ord($fourbytes[1]);
        $b2=ord($fourbytes[2]);
        $b3=ord($fourbytes[3]);
 
        $version_bits = ($b1 & 0x18) >> 3;
        $version = $versions[$version_bits];
        $simple_version =  ($version=='2.5' ? 2 : $version);
 
        $layer_bits = ($b1 & 0x06) >> 1;
        $layer = $layers[$layer_bits];
 
        $protection_bit = ($b1 & 0x01);
        $bitrate_key = sprintf('V%dL%d', $simple_version , $layer);
        $bitrate_idx = ($b2 & 0xf0) >> 4;
        $bitrate = isset($bitrates[$bitrate_key][$bitrate_idx]) ? $bitrates[$bitrate_key][$bitrate_idx] : 0;
 
        $sample_rate_idx = ($b2 & 0x0c) >> 2;//0xc => b1100
        $sample_rate = isset($sample_rates[$version][$sample_rate_idx]) ? $sample_rates[$version][$sample_rate_idx] : 0;
        $padding_bit = ($b2 & 0x02) >> 1;
        $private_bit = ($b2 & 0x01);
        $channel_mode_bits = ($b3 & 0xc0) >> 6;
        $mode_extension_bits = ($b3 & 0x30) >> 4;
        $copyright_bit = ($b3 & 0x08) >> 3;
        $original_bit = ($b3 & 0x04) >> 2;
        $emphasis = ($b3 & 0x03);
 
        $info = array();
        $info['Version'] = $version;//MPEGVersion
        $info['Layer'] = $layer;
        //$info['Protection Bit'] = $protection_bit; //0=> protected by 2 byte CRC, 1=>not protected
        $info['Bitrate'] = $bitrate;
        $info['Sampling Rate'] = $sample_rate;
        //$info['Padding Bit'] = $padding_bit;
        //$info['Private Bit'] = $private_bit;
        //$info['Channel Mode'] = $channel_mode_bits;
        //$info['Mode Extension'] = $mode_extension_bits;
        //$info['Copyright'] = $copyright_bit;
        //$info['Original'] = $original_bit;
        //$info['Emphasis'] = $emphasis;
        $info['Framesize'] = self::framesize($layer, $bitrate, $sample_rate, $padding_bit);
        $info['Samples'] = $samples[$simple_version][$layer];
        return $info;
    }
 
    private static function framesize($layer, $bitrate,$sample_rate,$padding_bit)
    {
        if ($layer==1)
            return intval(((12 * $bitrate*1000 /$sample_rate) + $padding_bit) * 4);
        else //layer 2, 3
            return intval(((144 * $bitrate*1000)/$sample_rate) + $padding_bit);
    }
}

$tokyoDB = new SQLite3("data/tokyo.db");
function sec2mmss($seconds) {
  $t = round($seconds);
  return sprintf('%02d:%02d', ($t/60%60), $t%60);
}

function mFilter($db, $pdb) {
	$ndb = array();
	foreach ($pdb as $id) {
		$ndb[$id] = $db[$id];
	}
	return $ndb;
}

switch($_GET["prompt"]) {
	case "upload":
		echo "
			<div class='container'>
				<h4>Upload from YouTube</h4>
				<hr />
				<p>Enter a YouTube URL below</p>
				<div class='form-group'>
					<input type='text' placeholder='http://youtube.com/watch?v=...' id='ytID' class='form-control' maxlength='43' />
				</div>
				<div class='form-group'>
					<input type='text' placeholder='Title' id='ytTitle' class='form-control' />
				</div>
				<div class='form-group'>
					<input type='text' placeholder='Artist (optional)' id='ytArtist' class='form-control' />
				</div>
				<div class='form-group'>
					<input type='text' placeholder='Description (optional)' id='ytDesc' class='form-control' />
				</div>
				<div class='form-group'>
					<input type='button' value='Upload' class='form-control' onclick=\"initUpload($('#ytID').val(), $('#ytTitle').val(), $('#ytArtist').val(), $('#ytDesc').val()); $.featherlight.current().close();\" />
				</div>
			</div>
		";
	break;
	case "upload2":
		$url = $_POST["url"];
		$ytID = substr(trim($url, " /?"), -11);
		if (file_exists("data/tokyo/{$ytID}.mp3")) {
			echo "File exists, cancelling.";
			die();
		}
		//$exec = '/usr/local/bin/youtube-dl --max-filesize 50m -x --audio-format mp3 ' . escapeshellarg($url) . ' -o "/var/www/tokyo.urusai.ninja/data/tokyo/%(id)s.%(ext)s"';
		$exec = '/usr/local/bin/youtube-dl --max-filesize 200m -x --no-playlist --audio-format mp3 --audio-quality 0 ' . escapeshellarg($url) . ' -o "/var/www/aftermirror.com/data/tokyo/%(id)s.%(ext)s"';
		exec($exec);
		if (!file_exists("data/tokyo/{$ytID}.mp3")) {
			// failed, try sproxy
			$fgc = file_get_contents("http://dsl.aftermirror.com/tokyo/sproxy.php?prompt=ytdl-passthru&ytID=" . $ytID);
			if (strlen($fgc) > 1024) {
				file_put_contents("data/tokyo/{$ytID}.mp3", $fgc);
				echo "[A] ";
			}
		}
		if (file_exists("data/tokyo/{$ytID}.mp3")) {
			/*
			$db = readDB("music.db");
			$db[$ytID] = array(
				"title" => $_POST["title"],
				"artist" => $_POST["artist"],
				"desc" => $_POST["desc"]
			);
			writeDB("music.db", $db);
			*/
			$mp3 = new MP3File("data/tokyo/{$ytID}.mp3");
			$duration = $mp3->getDuration();
			$stmt = $tokyoDB->prepare("INSERT INTO media (mediaID, title, artist, description, duration, uploader) VALUES (:a, :b, :c, :d, :e, :f);");
			$stmt->bindValue('a', $ytID, SQLITE3_TEXT);
			$stmt->bindValue('b', $_POST["title"], SQLITE3_TEXT);
			$stmt->bindValue('c', $_POST["artist"], SQLITE3_TEXT);
			$stmt->bindValue('d', $_POST["desc"], SQLITE3_TEXT);
			$stmt->bindValue('e', $duration, SQLITE3_INTEGER);
			$stmt->bindValue('f', "system", SQLITE3_TEXT);
			$stmt->execute();
			echo "Uploaded: {$_POST['title']} - {$_POST['artist']}";
			//
			$timeline = new Notify("system.timeline");
			$timeline->addNotification(array(
				"username" => AUTH_USER,
				"type" => "user",
				"icon" => "music",
				"header" => "uploaded a new song to tōkyō",
				"content" => "<b>{$_POST['title']}</b> &mdash; {$_POST['artist']}",
				"buttons" => array(
					array(
						"href" => "?app=Tokyo#{$ytID}",
						"text" => "Listen to this song",
						"class" => "primary"
					)
				),
				"adminOnly" => false
			));
		}
		else {
			echo "Failed to upload: {$_POST['title']} - {$_POST['artist']}";
		}
	break;
	case "playlists":
		echo "
			<div class='container'>
				<h4>Playlists</h4>
				<hr />
				<div style='text-align: center;'>
					<span class='btn btn-xs btn-primary' data-featherlight='ajax.php?prompt=create_playlist'>Create New Playlist</span>
				</div>
				<br />
				<ul class='nav nav-pills nav-stacked'>
		";
		$db = readDB("data/playlist.db");
		foreach ($db as $pID => $data) {
			$count = count($data["media"]);
			if ($count == 1) $count .= " song";
			else $count .= " songs";
			echo "<li><a href='?app=Tokyo&p={$pID}'>{$data['title']} ({$count})</a></li>";
		}
		echo "
				</ul>
			</div>
		";
	break;
	case "create_playlist":
		echo "
			<h4>Create Playlist</h4>
			<hr />
			<div class='form-group'>
				<input type='text' placeholder='Title' id='pTitle' class='form-control' />
			</div>
			<div class='form-group'>
				<input type='button' value='Upload' class='form-control' onclick=\"location.href = '?app=Tokyo&gp=' + $('#pTitle').val();\" />
			</div>
		";
	break;
	case "add":
		$pDB = readDB("data/playlist.db");
		$result = $tokyoDB->query("SELECT * FROM media") or die("Invalid database.");
		echo "
			<div class='container'>
				<h4>Add to {$pDB[$_GET['p']]['title']}</h4>
				<ul class='nav nav-pills nav-stacked'>
		";
		while ($data = $result->fetchArray()) {
			$ytID = $data["mediaID"];
			if (in_array($ytID, $pDB[$_GET["p"]]["media"])) continue;
			
			if ($data["title"] == "") $data["title"] = "(no title)";
			if ($data["artist"] == "") $data["artist"] = "(no artist)";
			if ($data["description"] == "") $data["description"] = "--";
			echo "<li class='addPBtn' media-id='{$ytID}'><a>{$data['title']} - {$data['artist']} ({$data['description']})</a></li>";
			//echo "<span class='addPBtn' media-id='{$ytID}'><span class='fa fa-plus'></span> {$data['title']} - {$data['artist']} ({$data['description']})</span>";
		}
		echo "
				</ul>
			</div>
		";
	break;
	case "catalog":
		$pDB;
		if (isSomething($_GET["p"])) {
			$pDB = readDB("data/playlist.db");
			if (isset($_GET["a"])) {
				$pDB[$_GET["p"]]["media"][] = $_GET["a"];
				writeDB("data/playlist.db", $pDB);
			}
		}
		
		$result = $tokyoDB->query("SELECT * FROM media") or die("Invalid database.");
		while ($data = $result->fetchArray()) {
			$ytID = $data["mediaID"];
			if (!file_exists("data/tokyo/{$ytID}.mp3")) continue;
			if (isSomething($_GET["p"]) && !in_array($ytID, $pDB[$_GET["p"]]["media"])) continue;
			if ($data["title"] == "") $data["title"] = "(no title)";
			if ($data["artist"] == "") $data["artist"] = "(no artist)";
			if ($data["description"] == "") $data["description"] = "--";
			$data["title"] = htmlentities($data["title"]);
			$data["artist"] = htmlentities($data["artist"]);
			$data["description"] = htmlentities($data["description"]);
			$q = implode(" ", array($data["title"], $data["artist"], $data["description"]));
			$sec = sec2mmss($data["duration"]);
			echo "
			<tr t-query=\"{$q}\">
				<td class='aTxt' media-id='{$ytID}' text-type='title'><span class='fa fa-play playBtn' media-id='{$ytID}'></span> {$data['title']}</td>
				<td class='aTxt' media-id='{$ytID}' text-type='artist'>{$data['artist']}</td>
				<td>{$sec}</td>
				<td class='aTxt' media-id='{$ytID}' text-type='description'>{$data['description']}</td>
				<!--<td>{$data['uploader']}</td>-->
			</tr>
			";
		}
		echo "
		<script>
			finishLoad();
		</script>
		";
	break;
	case "medit":
		$stmt = $tokyoDB->prepare("UPDATE media SET {$_POST['type']} = :a WHERE mediaID = :b");
		$stmt->bindValue('a', $_POST["value"], SQLITE3_TEXT);
		$stmt->bindValue('b', $_POST["id"], SQLITE3_TEXT);
		$stmt->execute();
		echo "Song updated.";
	break;
	case "dl":
		if (strlen($_GET["id"]) == 11) {
			$file = "data/tokyo/{$_GET['id']}.mp3";
			$result = $tokyoDB->query("SELECT * FROM media WHERE mediaID = '{$_GET['id']}'") or die("Invalid database.");
			$data = $result->fetchArray();
			
			header("Content-length: " . filesize($file));
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename="' . htmlentities($data["artist"]) . " - " . htmlentities($data["title"]) . '.mp3"');
			readfile($file);
			return true;
		}
	break;
}
?>