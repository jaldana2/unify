<?php
if (isset($_GET["ax"])) {
	setcookie("auth", $_GET["ax"], time() + (3600 * 24));
}
switch ($_GET["site"]) {
	case "welcome":
		$qs = base64_decode($_GET["qs"]);
		echo "
		<h1>welcome back, " . AUTH_USER . "</h1>
		<p>Returning you to your previous page... <span class='fa fa-spinner fa-spin'></span></p>
		<br />
		<a href='?{$qs}' class='btn btn-info'>Click me if you are not redirected within a few seconds</a>
		<script>
			setTimeout(\"location.href = '?{$qs}'\", 2500);
		</script>
		";
	break;
	case "urusai":
		$ax = $_COOKIE["auth"];
		echo "
		<p>Please wait... <span class='fa fa-spinner fa-spin'></span></p>
		<br />
		<a href='?app=Urusai' class='btn btn-info'>Click me if you are not redirected within a few seconds</a>
		<script>
			setTimeout(\"location.href = 'https://urusai.ninja/?app=passthru&ax={$ax}&site=p-urusai'\", 1650);
		</script>
		";
	break;
	case "aftermirror":
		$ax = $_COOKIE["auth"];
		echo "
		<p>Please wait... <span class='fa fa-spinner fa-spin'></span></p>
		<br />
		<a href='?app=Home' class='btn btn-info'>Click me if you are not redirected within a few seconds</a>
		<script>
			setTimeout(\"location.href = 'https://aftermirror.com/?app=passthru&ax={$ax}&site=p-aftermirror'\", 1650);
		</script>
		";
	break;
	case "p-urusai":
		echo "
		<p>Please wait... <span class='fa fa-spinner fa-spin'></span></p>
		<br />
		<a href='?app=Urusai' class='btn btn-info'>Click me if you are not redirected within a few seconds</a>
		<script>
			setTimeout(\"location.href = '?app=Urusai'\", 100);
		</script>
		";
	break;
	case "p-aftermirror":
		echo "
		<p>Please wait... <span class='fa fa-spinner fa-spin'></span></p>
		<br />
		<a href='?app=Home' class='btn btn-info'>Click me if you are not redirected within a few seconds</a>
		<script>
			setTimeout(\"location.href = '?app=Home'\", 100);
		</script>
		";
	break;
}