<?php
requireLogin();
define("PARENT_APP", "Urusai");

$urusai = new Urusai();
$id = $_GET["anime"];
if (!isset($id)) {
	echo "<script>location.href = '?app=Urusai';</script>";
	die();
}
$data = $urusai->getAnimeAttributes($id);

echo "
<div class='ani_block row'>
	<div class='col-xs-3' style='margin-top: 60px;'>
		<img src='?app=urusai-ajax&poster={$id}' class='urusai_poster' />
	</div>
	<div class='col-xs-9'>
		<h3>{$data['title']}</h3>
		<div class='well banner_text'>
";
if (file_exists("data/urusai/banner/{$id}.jpg")) {
	echo "
		<div style='background-image: url(?app=urusai-ajax&banner={$id});' class='urusai_banner' onclick=\"document.location = '?app=UrusaiPlayer&anime_id={$id}';\"></div>
	";
}
echo "
	{$data['synopsis']}
	</div>
";
$genres = explode(", ", $data["genres"]);
foreach ($genres as $genre_title) {
	echo "<span class='label label-primary'>{$genre_title}</span> ";
}
echo "
	</div>
</div>
<br />
";

$rid = substr(sha1(uniqid()), 0, 3);
$listing = array();

$episodes = $urusai->getEpisodes($id);
foreach ($episodes as $eid => $data) {
	$listing[$data['episode']][] = array(
		"id" => $eid,
		"quality" => $data["quality"],
		"src" => $data["src"]
	);
}
knatsort($listing);

$room = AUTH_USER . "." . substr(sha1(time()), 0, 3);
foreach ($listing as $episode => $links) {
	echo "<h4>Episode {$episode}</h4>";
	foreach ($links as $link) {
		echo "<span class='btn btn-lg btn-primary' data-featherlight='?app=urusai-ajax&eid={$link['id']}'>Episode {$episode} <b>{$link['quality']}</b></span> ";
	}
	echo "<a class='btn btn-lg btn-primary' href='?app=UrusaiSync&anime={$id}&ep={$episode}&room={$room}'>Invite Friends</b></a> ";
	echo "<br />";
}
?>