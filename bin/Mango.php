<?php
requireLogin();
define("PARENT_APP", "Mango");

$do = "default";
if (isset($_GET["do"])) $do = $_GET["do"];

switch ($do) {
	case "view":
		$manga = $_GET["manga"];
		$chapter = $_GET["chapter"];
		$dir = "data/mango/{$manga}/{$chapter}/";
		$length = count(dir_get($dir));
		
		echo "
			<script>
				var dir = \"{$dir}\";
				var length = {$length};
				var current = 1;
				var notificationSent = false;
				
				function loadPage(num) {
					if (num > 0 && num <= length) {
						$('#mangaImage').attr('src', 'images/loading.gif').attr('src', dir + num + '.jpg');
						$('#currentNum').text(num);
						if (num == 1) {
							preloadImages({$length});
						}
						else if (num == length) {
							markAsRead();
						}
					}
					else if (num <= 0) {
						current = 1;
					}
					else if (num > length) {
						current = length;
					}
				}
				
				/* mangaReader */
				function omvKeyPressed(e) {
					var keyCode = 0;
					if (navigator.appName == \"Microsoft Internet Explorer\") {
						if (!e) {
							var e = window.event;
						}
						if (e.keyCode) {
							keyCode = e.keyCode;
							if ((keyCode == 37) || (keyCode == 39)) {
								window.event.keyCode = 0;
							}
						} else {
							keyCode = e.which;
						}
					} else {
						if (e.which) {
							keyCode = e.which;
						} else {
							keyCode = e.keyCode;
						}
					}
					switch (keyCode) {
						case 37:
							current--;
							loadPage(current);
							return false;
						break;
						case 39:
							current++;
							loadPage(current);
							return false;
						break;
						default:
							return true;
						break;
					}
				}
				document.onkeydown = omvKeyPressed;
				
				function preloadImages(len) {
					for (var i = 2; i <= len; i++) {
						$('<img />').attr('src', dir + i + '.jpg');
					}
				}
				function markAsRead() {
					if (!notificationSent) {
						$.post('?app=notifications-ajax&mangoTracker', { series: \"{$manga}\", chapter: \"{$chapter}\" });
						notificationSent = true;
					}
				}
				$(function(e) {
					loadPage(current);
					$('.main-sidebar').hide();
					$('#navbar-resizer').attr('href', '?app=Mango');
					$('#navbar-resizer').text('  Go Back');
					$('#navbar-resizer').removeAttr('data-toggle');
				});
			</script>
			<style type='text/css'>
				@media screen and (max-width: 767px) {
					#mangaBox { top: 100px; }
				}
				@media screen and (min-width: 768px) {
					#mangaBox { top: 50px; }
				}
				#mangaBox { position: absolute; left: 0; right: 0; bottom: 0; background-color: #333E43; text-align: center; }
				#mangaMax { width: 100%; height: 90%; padding: 10px; position: relative; }
				#mangaImage { max-width: 100%; max-height: 100%; position: relative; top: 50%; transform: translateY(-50%); box-shadow: 0 0 4px black, 0 0 12px black; background: url(data/loading.gif) no-repeat; }
			</style>
		
			<div id='mangaBox'>
				<div id='mangaMax'>
					<img id='mangaImage' onclick=\"current++; loadPage(current);\" />
				</div>
				<div id='mangaControl' class='text-light-blue'>
					<i class='fa fa-arrow-left' onclick=\"current--; loadPage(current);\"></i>&nbsp;
					<span id='currentNum'>1</span> of {$length}
					<i class='fa fa-arrow-right' onclick=\"current++; loadPage(current);\"></i>
					<br />
					{$chapter}
				</div>
			</div>
		";
	break;
	default:
		// list
		
		echo "
			<div class='box box-solid'>
				<div class='box-header with-border'>
					<h4 class='box-title'>Manga Listing</h4>
				</div>
				<div class='box-body'>
					<div class='box-group' id='mangaListing'>
		";
		
		$listing = dir_get("data/mango");
		natsort($listing);
		foreach ($listing as $node) {
			$title = basename($node);
			$titleSafe = cleanANString($title);
			
			echo "
				<div class='panel box box-primary'>
					<div class='box-header with-border'>
						<h4 class='box-title'><a data-toggle='collapse' data-parent='#mangaListing' href='#manga{$titleSafe}'>{$title}</a></h4>
					</div>
					<div id='manga{$titleSafe}' class='panel-collapse collapse'>
						<div class='box-body'>
							<select class='cpicker form-control' base-url='?app=Mango&do=view&manga={$title}&chapter='>
								<option>Select chapter...</option>
			";
			
			$chapters = dir_get($node);
			foreach ($chapters as $chapter) {
				$ctitle = basename($chapter);
				echo "<option value='{$ctitle}'>{$ctitle}</option>";
			}
			
			echo "
							</select>
						</div>
					</div>
				</div>
				<script>
					$(function(e) {
						$('.cpicker').change(function(e) {
							location.href = $(this).attr('base-url') + $(this).val();
						});
					});
				</script>
			";
		}
		
		echo "
					</div>
				</div>
			</div>
		";
	break;
}
?>