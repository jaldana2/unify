<ul class="timeline">
	<?php
		date_default_timezone_set("America/Chicago");
		$timeline = new Notify("system.timeline");
		$gn = $timeline->getNotifications();
		$alot = false;
		if (count($gn) > 25) $alot = true;
		if (isset($_GET["full_timeline"])) {
			$gn = array_reverse($gn, true);
			$alot = false;
		}
		else {
			$gn = array_slice(array_reverse($gn, true), 0, 25, true);
		}
		$printedDates = array();
		foreach ($gn as $b) {
			if ($b["adminOnly"]) continue;
			$date = date("d M Y", $b["_sent"]);
			$time = date("H:i", $b["_sent"]);
			$time2 = time_since(time() - $b["_sent"]);
			if (!isset($printedDates[$date])) {
				$printedDates[$date] = true;
				echo "
					<li class='time-label'>
						<span class='bg-blue'>{$date}</span>
					</li>
				";
			}
			echo "
				<li>
					<i class='fa fa-{$b['icon']} bg-blue'></i>
					<div class='timeline-item'>
						<span class='time'><i class='fa fa-clock-o'></i> {$time} &mdash; {$time2} ago</span>
						<h3 class='timeline-header'><a href='#'>{$b['username']}</a> {$b['header']}</h3>
						<div class='timeline-body'>{$b['content']}</div>
						<div class='timeline-footer'>
			";
			if (is_array($b["buttons"]) && count($b["buttons"]) > 0) {
				foreach ($b["buttons"] as $button) {
					echo "
						<a class='btn btn-{$button['class']} btn-xs' href='{$button['href']}'>{$button['text']}</a>
					";
				}
			}
			echo "
						</div>
					</div>
				</li>
			";
		}
		if ($alot) {
			echo "
				<li class='time-label'>
					<span class='bg-blue' onclick=\"location.href = '?app=Home&full_timeline';\">View all...</span>
				</li>
			";
		}
	?>
	<li>
		<i class="fa fa-clock-o bg-gray"></i>
	</li>
</ul>