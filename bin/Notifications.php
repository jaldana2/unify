<?php
requireLogin();

$notify = new Notify(AUTH_USER);
$notifications = $notify->getNotifications();
if (count($notifications) > 0) foreach (array_reverse($notifications) as $id => $notification) {
	$timing = time_since(time() - $notification["_sent"]);
	$notify->readNotification($id);
	echo "
		<div class='box box-primary' id='notify-{$id}'>
			<div class='box-header'>
				<h4 class='box-title'><span class='fa fa-close notify-del-btn' style='color: red; cursor: pointer;' notifyID='{$id}'></span> <b>{$notification['title']}</b></h4>
				<br />
				<small>{$timing} ago</small>
			</div>
			<div class='box-body'>
				<p>{$notification['content']}</p>
	";
	if (isset($notification["link"])) {
		echo "
			<p><a href='{$notification['link']}' class='btn btn-sm btn-primary'>{$notification['link-title']}</a></p>
		";
	}
	echo "	</div>
		</div>
	";
}
else {
	echo "
		<div class='callout callout-info'>
			<h4>Nothing here!</h4>
			<p>You do not have any notifications.</p>
		</div>
	";
}

?>