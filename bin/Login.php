<?php
define("PARENT_APP", "Login");
$do = "login";
if (isset($_GET["do"])) $do = $_GET["do"];

switch($do) {
	case "login":
		echo "
		<form action='?app=Login&do=proc-login' method='post'>
			<h3>Login</h3>
			<div class='form-group'>
				<label>Username</label>
				<input type='text' name='username' placeholder='Username' class='form-control' id='login-username' />
			</div>
			<div class='form-group' id='group-password' style='display: none;'>
				<label>Password</label>
				<input type='password' name='password' placeholder='Password' class='form-control' id='login-password' />
			</div>
			<!--
			<div class='form-group'>
				<input type='checkbox' name='remember' checked />
				<label>Remember Me?</label>
			</div>
			-->
			<div class='form-group'>
				<input type='button' value='Login' class='form-control btn btn-primary' id='login-button' />
			</div>
		</form>
		<script>
			dest = '';
			$('#login-button').on('click', function(e) {
				var jqxhr = $.getJSON('?app=json&do=login&check-username=' + $('#login-username').val().toLowerCase() + '&check-password=' + $('#login-password').val(), function(data) {
					if (data['banned'] == 'yes') {
						$('#login-username').notify('This account has been banned.', { className: 'error', position: 'top' });
					}
					else if (data['valid'] == 'no') {
						$('#login-username').notify('Invalid username.', { className: 'error', position: 'top' });
					}
					else if (data['password'] == 'yes') {
						if (data['success'] == 'yes') {
							$('#login-username').notify('Please wait...', { className: 'success', position: 'top' });
							eLogin(data['xu'], data['xp']);
						}
						else if (data['success'] == 'no') {
							$('#login-password').notify('Incorrect password.', { className: 'warn', position: 'top' });
						}
						else {
							$('#login-username').notify('This is a password protected account.', { className: 'info', position: 'top' });
							$('#group-password').fadeIn(500);
							$('#login-password').focus();
						}
					}
					else {
						// no password
						$('#login-username').notify('Please wait...', { className: 'success', position: 'top' });
						eLogin(data['xu'], data['xp']);
					}
				});
			});
			function eLogin(username, password) {
				window.location = '?app=process&do=login&username=' + username + '&password=' + password + '&return=' + dest;
			}
			
			$(function() {
				$('#login-username, #login-password').on('keydown', function(e) {
					if (e.which == 13) {
						$('#login-button').click();
					}
				});
			});
		</script>
		<p>
			No account? <a href='?app=Register&do=register' class='label label-warning'>create a new account</a>
		</p>
		";
		if (isset($_GET["return"])) {
			echo "
			<script>
				dest = '{$_GET['return']}';
			</script>
			";
		}
	break;
}
?>