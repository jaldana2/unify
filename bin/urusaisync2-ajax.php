<?php
//enforceLogin();

if (isset($_GET["getAnimeEpisodes"])) {
	$urusai = new Urusai();
	$count = $urusai->getAnimeAttribute($_GET["getAnimeEpisodes"], "num_episodes");
	for ($i = 1; $i <= $count; $i++) {
		if (isset($_GET["s_ep"]) && $_GET["s_ep"] == $i) {
			echo "<option value='{$i}' selected>Episode {$i}</option>";
		}
		else {
			echo "<option value='{$i}'>Episode {$i}</option>";
		}
	}
}
elseif (isset($_GET["loadAnimeEpisode"])) {
	$urusai = new Urusai();
	$episode = $urusai->getEpisodeMirrors($_GET["loadAnimeEpisode"], $_GET["ep"]);
	//print_a($episode);
	$return = array();
	foreach ($episode as $id => $data) {
		if (!isset($return["HD"]) && $data["quality"] === "HD") {
			$return["src_high"] = $data["src"];
		}
		if (!isset($return["SD"]) && $data["quality"] === "SD") {
			$return["src_low"] = $data["src"];
		}
	}
	echo json_encode($return);
}
elseif (isset($_GET["settingsDialog"])) {
	echo "
	<div style='color: black !important;'>
		<input type='checkbox' id='prefer-hd-btn' onclick=\"prefer_hd = $(this).prop('checked');\"> Prefer HD?
		<br /><br />
		<span class='btn btn-xs btn-primary' onclick=\"$('.theatre').css('max-width', '1400px');\">Large Player</span> 
		<span class='btn btn-xs btn-primary' onclick=\"$('.theatre').css('max-width', '1000px');\">Medium Player (Default)</span> 
		<span class='btn btn-xs btn-primary' onclick=\"$('.theatre').css('max-width', '600px');\">Small Player</span>
		<br /><br />
		<span class='btn btn-xs btn-primary' onclick=\"$('body').css('background-color', 'black');\">Black Background</span>
		<span class='btn btn-xs btn-primary' onclick=\"$('body').css('background-color', '#222222');\">Gray Background</span>
		<span class='btn btn-xs btn-primary' onclick=\"$('.navbar').css('display', 'none');\">Hide Topbar</span>
		<span class='btn btn-xs btn-primary' onclick=\"$('.navbar').css('display', 'block');\">Show Topbar</span>
		<br /><br />
		<input type='checkbox' id='show-debug' /> <label for='show-debug'>Enable Developer Mode</label>
	</div>
	";
}
?>