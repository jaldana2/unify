<?php
enforceLogin();

$notify = new Notify(AUTH_USER);
$notifyMail = new Notify(AUTH_USER . ".mail");
if (isset($_GET["del"])) {
	$notify->removeNotification($_GET["del"]);
	echo 'rem?';
	print_a($notify->getNotifications());
}
elseif (isset($_GET["mangoTracker"])) {
	$d = dir_get("data/mango/{$_POST['series']}");
	$index = array_search("data/mango/{$_POST['series']}/{$_POST['chapter']}/", $d);
	if (isset($d[$index + 1])) {
		$next = basename($d[$index + 1]);
		$notify->addNotification(array(
			"title" => "Mango",
			"icon" => "fa-lemon-o",
			"content" => "Finished: {$_POST['chapter']} ({$_POST['series']})",
			"link" => "?app=Mango&do=view&manga={$_POST['series']}&chapter={$next}",
			"link-title" => "Continue Reading",
			"sender" => "system-mango"
		));
	}
	else {
		$notify->addNotification(array(
			"title" => "Mango",
			"icon" => "fa-lemon-o",
			"content" => "Completed {$_POST['series']}",
			"link" => "?app=Mango",
			"link-title" => "Read More",
			"sender" => "system-mango"
		));
	}
}
else {
	echo $notify->countUnreadNotifications() . "," . $notifyMail->countUnreadNotifications();
}
?>