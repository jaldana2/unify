<?php
enforceLogin();
define("PARENT_APP", "Urusai");
$urusai = new Urusai();

echo "
	<div class='box'>
		<div class='box-header'>
			<h3 class='box-title'>Tasks</h3>
		</div>
		<div class='box-body'>
			<a class='btn btn-app' href='?app=UrusaiManager'>
				<i class='fa fa-home'></i>
				Home
			</a>
			<a class='btn btn-app' href='?app=UrusaiManager&do=add'>
				<i class='fa fa-plus'></i>
				Add
			</a>
			<a class='btn btn-app' href='?app=UrusaiManager&do=resync'>
				<i class='fa fa-refresh'></i>
				Resync
			</a>
		</div>
	</div>
	<div class='box'>
		<div class='box-body'>
";

$do = "default";
if (isset($_GET["do"])) $do = $_GET["do"];

switch ($do) {
	case "add":
		if (isset($_POST["title"])) {
			$urusai->addAnime(
				htmlentities($_POST["title"]),
				htmlentities($_POST["alt_title"]),
				$_POST["air_season"],
				$_POST["num_episodes"],
				$_POST["genres"],
				$_POST["rating"],
				$_POST["synopsis"],
				$_POST["airing"],
				$_POST["status"]
			);
			$anime_id = count($urusai->getListing());
			if (isset($_FILES["banner"]) && $_FILES["banner"]["size"] > 0) {
				if (file_exists("data/urusai/banner/{$anime_id}.jpg")) unlink("data/urusai/banner/{$anime_id}.jpg");
				iTF($_FILES["banner"]["tmp_name"], "data/urusai/banner/{$anime_id}.jpg", 480, 89);
			}
			if (isset($_FILES["poster"]) && $_FILES["poster"]["size"] > 0) {
				if (file_exists("data/urusai/poster/{$anime_id}.jpg")) unlink("data/urusai/poster/{$anime_id}.jpg");
				iTF($_FILES["poster"]["tmp_name"], "data/urusai/poster/{$anime_id}.jpg", 920, 86);
			}
			echo "
				<div class='callout callout-success'>
					<h4>Yay!</h4>
					<p>Anime added successfully.</p>
				</div>
			";
			$timeline = new Notify("system.timeline");
			$timeline->addNotification(array(
				"username" => AUTH_USER,
				"type" => "system",
				"icon" => "cloud-upload",
				"header" => "added a new series on urusai",
				"content" => "<b>{$_POST['title']}</b> &mdash; has recently been added.",
				"buttons" => array(
					array(
						"href" => "?app=UrusaiPlayer&anime={$anime_id}",
						"text" => "More info",
						"class" => "primary"
					)
				),
				"adminOnly" => false
			));
		}
		echo "
			<form role='form' action='?app=UrusaiManager&do=add' method='post' enctype='multipart/form-data'>
				<div class='form-group'>
					<label for='anime_title'>Title</label>
					<input type='text' name='title' id='anime_title' placeholder='(required)' class='form-control' />
				</div>
				<div class='form-group'>
					<label for='anime_alt_title'>Alternate Title(s)</label>
					<input type='text' name='alt_title' id='anime_alt_title' placeholder='if available' class='form-control' />
				</div>
				<div class='form-group'>
					<label for='anime_air_season'>Airing Season</label>
					<input type='text' name='air_season' id='anime_air_season' placeholder='e.g. Winter 2016' class='form-control' />
				</div>
				<div class='form-group'>
					<label for='anime_num_episodes'>Number of Episodes</label>
					<input type='text' name='num_episodes' id='anime_num_episodes' placeholder='' class='form-control' />
				</div>
				<div class='form-group'>
					<label for='anime_genres'>Genre(s)</label>
					<input type='text' name='genres' id='anime_genres' placeholder='e.g. Action, Shounen, Slice of Life' class='form-control' />
				</div>
				<div class='form-group'>
					<label for='anime_rating'>Rating</label>
					<input type='text' name='rating' id='anime_rating' placeholder='e.g. R - 17+ (violence & profanity)' class='form-control' />
				</div>
				<div class='form-group'>
					<label for='anime_synopsis'>Synopsis</label>
					<textarea id='anime_synopsis' name='synopsis' style='width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;'></textarea>
				</div>
				<div class='form-group'>
					<label for='anime_airing'>Airing? (0 = false, 1 = true)</label>
					<input type='text' name='airing' id='anime_airing' placeholder='' class='form-control' />
				</div>
				<div class='form-group'>
					<label for='anime_status'>Status? (0 = unavailable, 1 = available)</label>
					<input type='text' name='status' id='anime_status' placeholder='' value='1' class='form-control' />
				</div>
				<div class='form-group'>
					<label for='anime_banner'>Banner</label>
					<input type='file' class='form-control' name='banner' placeholder='Banner' />
				</div>
				<div class='form-group'>
					<label for='anime_banner'>Poster</label>
					<input type='file' class='form-control' name='poster' placeholder='Poster' />
				</div>
				<div class='form-group'>
					<input type='submit' value='Save' class='form-control btn btn-primary' />
				</div>
			</form>
			<script>
				$('#anime_synopsis').wysihtml5();
			</script>
		";
	break;
	case "edit":
		echo "
			<a class='btn btn-block btn-primary btn-social' href='?app=UrusaiManager&do=episodes&anime={$_GET['anime']}'><i class='fa fa-edit'></i> Edit Episodes</a>
			<br />
		";
		if ($_GET["anime"] > 1) {
			echo "<a href='?app=UrusaiManager&do=edit&anime=" . ($_GET["anime"] - 1) . "' class='btn btn-lg btn-primary'>Previous</a> ";
		}
		if ($urusai->getAnimeAttributes($_GET["anime"] + 1)) {
			echo "<a href='?app=UrusaiManager&do=edit&anime=" . ($_GET["anime"] + 1) . "' class='btn btn-lg btn-primary'>Next</a> ";
		}
		echo "<br /><br />";
		if (isset($_POST["title"])) {
			$data = $urusai->getAnimeAttributes($_GET["anime"]);
			foreach ($_POST as $k => $v) {
				if (isset($data[$k]) && $data[$k] !== $v) {
					switch ($k) {
						case "title":
						case "alt_title":
						case "air_season":
						case "genres":
						case "rating":
						case "synopsis":
							$urusai->setAnimeAttribute($_GET["anime"], $k, $_POST[$k], SQLITE3_TEXT);
						break;
						case "num_episodes":
						case "airing":
						case "status":
							$urusai->setAnimeAttribute($_GET["anime"], $k, $_POST[$k], SQLITE3_INTEGER);
						break;
					}
				}
			}
			if (isset($_FILES["banner"]) && $_FILES["banner"]["size"] > 0) {
				if (file_exists("data/urusai/banner/{$_GET['anime']}.jpg")) unlink("data/urusai/banner/{$_GET['anime']}.jpg");
				iTF($_FILES["banner"]["tmp_name"], "data/urusai/banner/{$_GET['anime']}.jpg", 480, 89);
			}
			if (isset($_FILES["poster"]) && $_FILES["poster"]["size"] > 0) {
				if (file_exists("data/urusai/poster/{$_GET['anime']}.jpg")) unlink("data/urusai/poster/{$_GET['anime']}.jpg");
				iTF($_FILES["poster"]["tmp_name"], "data/urusai/poster/{$_GET['anime']}.jpg", 920, 86);
			}
			echo "
				<div class='callout callout-success'>
					<h4>Yay!</h4>
					<p>Anime updated successfully.</p>
				</div>
			";
		}
		$mirrors = $urusai->getEpisodeMirrors($_GET["anime"], 1);
		if (!count($mirrors)) {
			echo "
				<div class='callout callout-danger'>
					<h4>Oh no!</h4>
					<p>Anime does not have any mirrors for episode 1, perhaps you should set this to status = 0.</p>
				</div>
			";
		}
		$data = $urusai->getAnimeAttributes($_GET["anime"]);
		echo "
			<form role='form' action='?app=UrusaiManager&do=edit&anime={$_GET['anime']}' method='post' enctype='multipart/form-data'>
				<div class='form-group'>
					<label for='anime_title'>Title</label>
					<input type='text' name='title' id='anime_title' placeholder='(required)' class='form-control' value='{$data['title']}' />
				</div>
				<div class='form-group'>
					<label for='anime_alt_title'>Alternate Title(s)</label>
					<input type='text' name='alt_title' id='anime_alt_title' placeholder='if available' class='form-control' value='{$data['alt_title']}' />
				</div>
				<div class='form-group'>
					<label for='anime_air_season'>Airing Season</label>
					<input type='text' name='air_season' id='anime_air_season' placeholder='e.g. Winter 2016' class='form-control' value='{$data['air_season']}' />
				</div>
				<div class='form-group'>
					<label for='anime_num_episodes'>Number of Episodes</label>
					<input type='text' name='num_episodes' id='anime_num_episodes' placeholder='' class='form-control' value='{$data['num_episodes']}' />
				</div>
				<div class='form-group'>
					<label for='anime_genres'>Genre(s)</label>
					<input type='text' name='genres' id='anime_genres' placeholder='e.g. Action, Shounen, Slice of Life' class='form-control' value='{$data['genres']}' />
				</div>
				<div class='form-group'>
					<label for='anime_rating'>Rating</label>
					<input type='text' name='rating' id='anime_rating' placeholder='e.g. R - 17+ (violence & profanity)' class='form-control' value='{$data['rating']}' />
				</div>
				<div class='form-group'>
					<label for='anime_synopsis'>Synopsis</label>
					<textarea id='anime_synopsis' name='synopsis' style='width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;'>{$data['synopsis']}</textarea>
				</div>
				<div class='form-group'>
					<label for='anime_airing'>Airing? (0 = false, 1 = true)</label>
					<input type='text' name='airing' id='anime_airing' placeholder='' class='form-control' value='{$data['airing']}' />
				</div>
				<div class='form-group'>
					<label for='anime_status'>Status? (0 = unavailable, 1 = available)</label>
					<input type='text' name='status' id='anime_status' placeholder='' class='form-control' value='{$data['status']}' />
				</div>
				<div class='form-group'>
					<label for='anime_banner'>Banner</label>
					<input type='file' class='form-control' name='banner' placeholder='Banner' />
				</div>
				<div class='form-group'>
					<label for='anime_banner'>Poster</label>
					<input type='file' class='form-control' name='poster' placeholder='Poster' />
				</div>
				<div class='form-group'>
					<input type='submit' value='Update' class='form-control btn btn-primary' />
				</div>
			</form>
			<script>
				$('#anime_synopsis').wysihtml5();
			</script>
		";
	break;
	case "episodes":
		$title = $urusai->getAnimeAttribute($_GET["anime"], "title");
		echo "<h4>{$title}</h4>";
		echo "
			<a class='btn btn-block btn-primary btn-social' href='?app=UrusaiManager&do=scraper&anime={$_GET['anime']}'><i class='fa fa-edit'></i> Edit Scraper Listing for this Anime</a>
			<a class='btn btn-block btn-primary btn-social' href='?app=UrusaiManager&do=KAHelper&anime={$_GET['anime']}'><i class='fa fa-edit'></i> Paste kissanime Links</a>
			<a class='btn btn-block btn-primary btn-social' href='?app=UrusaiManager&do=edit&anime={$_GET['anime']}'><i class='fa fa-edit'></i> Edit Anime Information</a>
			<br />
		";
		if (isset($_GET["purge"])) {
			$urusai->purgeEpisodes($_GET["anime"]);
			echo "
				<div class='callout callout-success'>
					<h4>Success!</h4>
					<p>All episodes purged.</p>
				</div>
			";
		}
		elseif (isset($_POST["src"])) {
			$urusai->addEpisode($_GET["anime"], $_POST["episode"], $_POST["quality"], $_POST["src"]);
			echo "
				<div class='callout callout-success'>
					<h4>Success!</h4>
					<p>Episode added.</p>
				</div>
			";
		}
		elseif (isset($_GET["delete"])) {
			$urusai->removeEpisode($_GET["delete"]);
			echo "
				<div class='callout callout-success'>
					<h4>Success!</h4>
					<p>Episode deleted.</p>
				</div>
			";
		}
		$episodes = $urusai->getEpisodes($_GET["anime"]);
		echo "
			<form action='?app=UrusaiManager&do=episodes&anime={$_GET['anime']}' method='post'>
			<table class='table table-hover'>
				<tr>
					<th style='width: 94px;'>eID</th>
					<th style='width: 64px;'>#</th>
					<th style='width: 64px;'>Q</th>
					<th>Source</th>
				</tr>
		";
		foreach ($episodes as $id => $data) {
			$tclass = "";
			if ($data["quality"] === "HD") $tclass = "text-light-blue";
			echo "
				<tr class='{$tclass}'>
					<td><a href='?app=UrusaiManager&do=episodes&anime={$_GET['anime']}&delete={$id}' class='label label-danger'><i class='fa fa-close'></i></a> {$id}</td>
					<td>{$data['episode']}</td>
					<td>{$data['quality']}</td>
					<td>{$data['src']}</td>
				</tr>";
		}
		echo "
				<tr>
					<td>Add:</td>
					<td><input type='text' placeholder='#' name='episode' style='width: 48px;'  /></td>
					<td><input type='text' placeholder='Q' name='quality' style='width: 54px;' /></td>
					<td><input type='text' placeholder='Source' name='src' style='width: 80%;' /> <input type='submit' class='btn btn-primary' value='Add' /></td>
				</tr>
		";
		echo "
			</table>
			</form>
		";
		echo "
			<br />
			<span class='btn btn-xs btn-danger' onclick=\"$(this).hide(); $('#purgeAllEpisodeBtn').fadeIn(200);\">Purge all links</span>
			<a class='btn btn-block btn-danger btn-social' href='?app=UrusaiManager&do=episodes&anime={$_GET['anime']}&purge' id='purgeAllEpisodeBtn' style='display: none;'><i class='fa fa-close'></i> <b>Confirm:</b> Purge all Links</a>
		";
	break;
	case "KAHelper":
		$title = $urusai->getAnimeAttribute($_GET["anime"], "title");
		echo "<h4>{$title}</h4>";
		if (isset($_POST["values"])) {
			$values = explode("\n", $_POST["values"]);
			$flags = explode(",", $_POST["flags"]);
			foreach ($values as $i => $episode) {
				$num = $i + 1; // index starts at 0
				$episode = trim($episode);
				
				$base = substr($episode, 0, -2);
				
				$data = array();
				if (in_array("1080", $flags)) {
					$urusai->addEpisode($_GET["anime"], $num, "HD1080", $base . "37");
				}
				if (in_array("720", $flags)) {
					$urusai->addEpisode($_GET["anime"], $num, "HD", $base . "22");
				}
				if (in_array("360", $flags)) {
					$urusai->addEpisode($_GET["anime"], $num, "SD", $base . "18");
				}
			}
			echo "
				<div class='callout callout-success'>
					<h4>Success!</h4>
					<p>KA list imported.</p>
				</div>
			";
		}
		echo "
			<h4>Paste kissanime Links</h4>
			<form role='form' action='?app=UrusaiManager&do=KAHelper&anime={$_GET['anime']}' method='post'>
				<div class='form-group'>
					<label>URLs, separated by new lines.</label>
					<textarea class='form-control' name='values' rows='24' placeholder='http://2.bp.blogspot.com/...=m22\nhttp://2.bp.blogspot.com/...=m22\n...'></textarea>
				</div>
				<div class='form-group'>
					<label>Flags (if 1080p not available, remove '1080,')</label>
					<input type='text' class='form-control' name='flags' value='1080,720,360'>
				</div>
				<div class='form-group'>
					<input type='submit' value='Save' class='form-control btn btn-primary' />
				</div>
			</form>
		";
	break;
	case "scraper":
		$title = $urusai->getAnimeAttribute($_GET["anime"], "title");
		echo "<h4>{$title}</h4>";
		$scraper = readDB("data/scraper.db");
		if (isset($_POST["base_url"])) {
			$scraper[$_GET["anime"]] = array(
				"base_url" => $_POST["base_url"],
				"template" => $_POST["template"],
				"status" => $_POST["status"]
			);
			writeDB("data/scraper.db", $scraper);
			echo "
				<div class='callout callout-success'>
					<h4>Yay!</h4>
					<p>Scraper updated successfully.</p>
				</div>
			";
		}
		if (isset($scraper[$_GET["anime"]])) {
			// exists
			$data = $scraper[$_GET["anime"]];
			echo "
				<h4>Edit Scraper</h4>
				<form role='form' action='?app=UrusaiManager&do=scraper&anime={$_GET['anime']}' method='post'>
					<div class='form-group'>
						<label for='anime_base_url'>masterani.me/animeultima Base URL</label>
						<input type='text' name='base_url' id='anime_base_url' placeholder='e.g. http://www.masterani.me/anime/watch/27-fairy-tail-2014/ or http://www.animeultima.io/watch/Dimension-W-english-subbed-dubbed-online/' class='form-control' value='{$data['base_url']}' />
					</div>
					<div class='form-group'>
						<label for='anime_template'>Filename Template</label>
						<input type='text' name='template' id='anime_template' placeholder='(required)' class='form-control' value='{$data['template']}' />
					</div>
					<div class='form-group'>
						<label for='anime_status'>Status? (0 = disabled, 1 = enabled)</label>
						<input type='text' name='status' id='anime_status' placeholder='' class='form-control' value='{$data['status']}' />
					</div>
					<div class='form-group'>
						<input type='submit' value='Save' class='form-control btn btn-primary' />
					</div>
				</form>
			";
		}
		else {
			// not exists
			echo "
				<h4>Add to Scraper</h4>
				<form role='form' action='?app=UrusaiManager&do=scraper&anime={$_GET['anime']}' method='post'>
					<div class='form-group'>
						<label for='anime_base_url'>masterani.me Base URL</label>
						<input type='text' name='base_url' id='anime_base_url' placeholder='e.g. http://www.masterani.me/anime/watch/27-fairy-tail-2014/' class='form-control' />
					</div>
					<div class='form-group'>
						<label for='anime_template'>Filename Template</label>
						<input type='text' name='template' id='anime_template' placeholder='(required)' class='form-control' value='data/urusai/%quality%/%id%-%episode%.mp4' />
					</div>
					<div class='form-group'>
						<label for='anime_status'>Status? (0 = disabled, 1 = enabled)</label>
						<input type='text' name='status' id='anime_status' placeholder='' value='1' class='form-control' />
					</div>
					<div class='form-group'>
						<input type='submit' value='Save' class='form-control btn btn-primary' />
					</div>
				</form>
			";
		}
	break;
	case "default":
		$anime = $urusai->getListing();
		echo "
			<table class='table table-hover'>
				<tr>
					<th style='width: 100px;'>Episodes</th>
					<th>Anime</th>
				</tr>
		";
		foreach ($anime as $id => $title) {
			echo "
				<tr>
					<td><a href='?app=UrusaiManager&do=episodes&anime={$id}' class='btn btn-xs btn-primary'>Episodes</a></td>
					<td><a href='?app=UrusaiManager&do=edit&anime={$id}'>{$title}</a></td>
				</tr>
			";
		}
		echo "
			</table>
		";
	break;
	default:
		echo "
			<div class='callout callout-danger'>
				<h4>Error</h4>
				<p>Invalid task.</p>
			</div>
		";
	break;
}

echo "
		</div>
	</div>
";
?>