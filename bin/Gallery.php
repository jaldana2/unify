<?php
define("PARENT_APP", "Gallery");
$root = "data/gallery";
$gallery = new Gallery();

echo "
	<style type='text/css'>
		.gallery-thumb { height: 200px; background-size: cover; background-position: center center; transition: box-shadow 0.25s; margin-bottom: 20px; position: relative; cursor: pointer; }
		.gallery-thumb:hover { box-shadow: 0 0 4px black, 0 0 8px black; z-index: 999; }
	</style>
";

if (isset($_GET["do"]) && $_GET["do"] == "view") {
	echo "
		<a href='?app=Gallery' class='btn btn-block btn-default'>Gallery Home</a>
		<br />
	";
}
else {
	echo "
		<div class='box'>
			<div class='box-header'>
				<h4 class='box-title'>Menu</h4>
			</div>
			<div class='box-body'>
				<a class='btn btn-app' href='?app=Gallery'>
					<i class='fa fa-image'></i>
					Home
				</a>
				<a class='btn btn-app' href='?app=Gallery&do=new'>
					<i class='fa fa-plus'></i>
					New
				</a>
			</div>
		</div>
	";	
}

$do = "default";
if (isset($_GET["do"])) $do = $_GET["do"];

switch ($do) {
	case "new":
		if (isset($_POST["cName"])) {
			$id = count($gallery->getCollections()) + 1;
			$gallery->createCollection(htmlentities($_POST["cName"]), $_POST["cDesc"], AUTH_USER);
			echo "
				<div class='callout callout-success'>
					<h4>Yes!</h4>
					<p>You have successfully created a new collection.</p>
					<p><a href='?app=Gallery&do=view&gallery={$id}' class='btn btn-info'>Click here to continue</a></p>
				</div>
			";
		}
		echo "
			<div class='box box-primary'>
				<div class='box-header'>
					<h4 class='box-title'>New Collection</h4>
				</div>
				<div class='box-body'>
					<form action='?app=Gallery&do=new' method='post' role='form' class='form'>
						<div class='form-group'>
							<label for='cName'>Collection Name</label>
							<input type='text' id='cName' name='cName' placeholder='(required)' class='form-control' />
						</div>
						<div class='form-group'>
							<label for='cDesc'>Collection Description</label>
							<textarea id='cDesc' name='cDesc' rows='6' class='form-control'></textarea>
						</div>
						<div class='form-group'>
							<input type='submit' value='Submit' class='form-control btn btn-primary' />
						</div>
					</form>
					<script>
						$('#cDesc').wysihtml5();
					</script>
				</div>
			</div>
		";
	break;
	case "view":
		$info = $gallery->getCollectionAttributes($_GET["gallery"]);
		if (defined("AUTH_USER") && $info["creator"] === AUTH_USER) {
			if (!empty($_FILES)) {
				$tmp_name = $_FILES["file"]["tmp_name"];
				$file = AUTH_USER . sha1(uniqid() . AUTH_USER) . ".jpg";
				if (!is_dir("data/gallery/")) {
					mkdir("data/gallery/");
					mkdir("data/gallery/thumbs/");
					mkdir("data/gallery/src/");
				}
				$th_target = "data/gallery/thumbs/{$file}";
				$sr_target = "data/gallery/src/{$file}";
				iTF($tmp_name, $th_target, 500, 88);
				iTF($tmp_name, $sr_target, 2000, 91);
				$gallery->addMedia($_GET["gallery"], $file, "", "");
				echo "
				<div class='callout callout-success'>
					<h4>Yes!</h4>
					<p>You have uploaded a new file to your collection.</p>
				</div>
				";
			}
			echo "
				<div class='box'>
					<div class='box-header'>
						<h4 class='box-title'>Menu</h4>
					</div>
					<div class='box-body'>
						<a class='btn btn-app' id='gallery-upl-btn' href='#'>
							<i class='fa fa-plus'></i>
							Add
						</a>
						<a class='btn btn-app' href='?app=Gallery&do=edit&gallery={$_GET['gallery']}'>
							<i class='fa fa-edit'></i>
							Edit
						</a>
					</div>
				</div>
				<div style='display: none;'>
					<form action='?app=Gallery&do=view&gallery={$_GET['gallery']}' method='post' enctype='multipart/form-data' id='file-upl'>
						<input type='file' name='file' id='file'>
					</form>
				</div>
				<script>
					$('#file').change(function() {
						$('#file-upl').submit();
					});
					$('#gallery-upl-btn').on('click', function() {
						$('#file').click();
					});
				</script>
			";
		}
		echo "
			<h2 class='page-header'>&quot;{$info['title']}&quot; by {$info['creator']}</h2>
		";
		if (strlen($info["description"]) > 0) {
			echo "<blockquote>{$info['description']}</blockquote>";
		}
		
		$media = $gallery->getCollectionMedia($_GET["gallery"]);
		if (count($media) === 0) {
			echo "
				<div class='callout callout-info'>
					<h4>Oh no!</h4>
					<p>There isn't anything here yet.</p>
				</div>
			";
		}
		else {
			echo "
				<div class='row'>
			";
			
			foreach (array_reverse($media) as $id => $data) {
				echo "
					<div class='col-xs-4 col-sm-3 col-md-3'>
						<div class='gallery-thumb' style='background-image: url(data/gallery/thumbs/{$data['src']});' data-featherlight='data/gallery/src/{$data['src']}'>
						</div>
					</div>
				";
			}
			
			echo "
				</div>
			";
		}
	break;
	case "edit":
		$info = $gallery->getCollectionAttributes($_GET["gallery"]);
		if ($info["creator"] !== AUTH_USER) {
			echo "<script>location.href = '?app=Gallery';</script>";
			echo "<p>Not allowed.</p>";
		}
		else {
			if (isset($_POST["cName"])) {
				$gallery->setCollectionAttribute($_GET["gallery"], "title", htmlentities($_POST["cName"]), SQLITE3_TEXT);
				$gallery->setCollectionAttribute($_GET["gallery"], "description", $_POST["cDesc"], SQLITE3_TEXT);
				$info = $gallery->getCollectionAttributes($_GET["gallery"]);
				echo "
					<div class='callout callout-success'>
						<h4>Yes!</h4>
						<p>You have successfully updated your collection.</p>
						<p><a href='?app=Gallery&do=view&gallery={$_GET['gallery']}' class='btn btn-info'>Click here to continue</a></p>
					</div>
				";
			}
			echo "
				<div class='box box-primary'>
					<div class='box-header'>
						<h4 class='box-title'>Edit Collection</h4>
					</div>
					<div class='box-body'>
						<form action='?app=Gallery&do=edit&gallery={$_GET['gallery']}' method='post' role='form' class='form'>
							<div class='form-group'>
								<label for='cName'>Collection Name</label>
								<input type='text' id='cName' name='cName' placeholder='(required)' value=\"" . htmlentities($info["title"]) . "\" class='form-control' />
							</div>
							<div class='form-group'>
								<label for='cDesc'>Collection Description</label>
								<textarea id='cDesc' name='cDesc' rows='6' class='form-control'>{$info['description']}</textarea>
							</div>
							<div class='form-group'>
								<input type='submit' value='Update' class='form-control btn btn-primary' />
							</div>
						</form>
						<script>
							$('#cDesc').wysihtml5();
						</script>
					</div>
				</div>
			";
		}
	break;
	default:
		$collections = $gallery->getCollections();
		if (count($collections) === 0) {
			echo "
				<div class='callout callout-info'>
					<h4>Oh no!</h4>
					<p>There isn't anything here yet.</p>
				</div>
			";
		}
		else {
			echo "
				<div class='row'>
				<style type='text/css'>
					.gallery-title { position: absolute; bottom: 0; left: 0; right: 0; padding: 8px; background-color: rgba(0, 0, 0, 0.75); color: white; }
				</style>
			";
			foreach ($collections as $id => $title) {
				$media = $gallery->getCollectionMedia($id);
				$info = $gallery->getCollectionAttributes($id);
				if (count($media) > 0) {
					$banner = array_pop($media);
					
					echo "
						<div class='col-xs-6 col-sm-6 col-md-4'>
							<div class='gallery-thumb' style='background-image: url(data/gallery/thumbs/{$banner['src']});' onclick=\"location.href = '?app=Gallery&do=view&gallery={$id}';\">
								<span class='gallery-title'><b>{$title}</b> &mdash; {$info['creator']}</span>
							</div>
						</div>
					";
				}
				else {
					echo "
						<div class='col-xs-6 col-sm-6 col-md-4'>
							<div class='gallery-thumb' onclick=\"location.href = '?app=Gallery&do=view&gallery={$id}';\">
								<span class='gallery-title'><b>{$title}</b> &mdash; {$info['creator']}</span>
							</div>
						</div>
					";
				}
			}
			echo "
				</div>
			";
		}
	break;
}
?>