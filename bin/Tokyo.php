<?php requireLogin(); define("PARENT_APP", "Tokyo"); ?>
<style type="text/css">
	.center { text-align: center; }
	.navbar { margin-bottom: 0; }
	.navbar-header { padding: 15px; float: left; text-align: center; width: 100%; }
	#hBack { }
	#hForward { }
	.navbar-brand { float: none; margin: 0 !important; }
	.aTxt { -webkit-user-select: none; -moz-user-select: none; -khtml-user-select: none; -ms-user-select: none; }
	#mediaLibraryContainer { margin-bottom: 200px; width: 100%; position: relative; }
	td { overflow: hidden; text-overflow: ellipsis; white-space: nowrap; }
	table { table-layout: fixed; }
	.playBtn { width: 24px; }
	.hidden { display: none; }
	#hTitle { max-height: 32px; }
	#mediaPlayer { width: 70%; max-width: 640px; }
	#mediaPlayerContainer { position: fixed; bottom: 0; left: 0; right: 0; z-index: 999; box-sizing: border-box; background-color: #333333; text-align: center; color: white; padding: 12px 20px; overflow: hidden; height: 51px; -webkit-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none; }
	#mediaPlayerContainer .fa { transition: color 0.5s ease; padding: 6px; }
	#mediaPlayerContainer .fa:hover { color: #008cba; }
	#mediaPlayerContainer input[type="range"] { position: relative; top: 2px; }
	@media only screen and (max-width: 600px) {
		.optional { display: none; }
	}
	
	input[type="range"] {
	  -webkit-appearance: none !important;
	  width: 100%;
	  height: 15px;
	  background-color: #efefef;
	  border: 1px solid #dbdbdb;
	  border-radius: 10px;
	  margin: auto;
	  transition: all 0.3s ease;
	  outline: none;
	}
	input[type="range"]:hover {
	  background-color: #dddddd;
	}

	input[type="range"]::-webkit-slider-thumb {
	  -webkit-appearance: none !important;
	  width: 22px;
	  height: 22px;
	  background-color: #006687;
	  border-radius: 30px;
	  box-shadow: 0px 0px 3px #004b63;
	  transition: all 0.5s ease;
	}
	input[type="range"]::-webkit-slider-thumb:hover {
	  background-color: #005576;
	}
	input[type="range"]::-webkit-slider-thumb:active {
	  box-shadow: 0px 0px 1px #004b63;
	}
</style>

<div class="container-fluid" id="control">
	<span class='btn btn-sm btn-primary' data-featherlight='?app=tokyo-ajax&prompt=upload'><span class="fa fa-plus"></span></span>
	<?php
	if (isset($_GET["p"])) {
		echo "
			<span class='btn btn-sm btn-primary' data-featherlight='?app=tokyo-ajax&prompt=add&p={$_GET['p']}'>Add to Playlist</span>
		";
	}
	?>
	<span class="btn btn-sm btn-primary" data-featherlight="?app=tokyo-ajax&prompt=playlists">Playlists</span> 
	<label for='optRepeat' class='btn btn-sm btn-info cbCtr'><input type='checkbox' id='optRepeat' /> &nbsp;<span class='fa fa-repeat'></span></label>
	<label for='optShuffle' class='btn btn-sm btn-info cbCtr'><input type='checkbox' id='optShuffle' /> &nbsp;<span class='fa fa-random'></span></label>
	<label for='optAutoplay' class='btn btn-sm btn-info cbCtr'><input type='checkbox' id='optAutoplay' checked /> &nbsp;<span class='fa fa-play-circle'></span></label>
</div>
<br />
<div id="mediaLibraryContainer">
	<?php
	if (isset($_GET["p"])) {
		$pDB = readDB("data/playlist.db");
		echo "<h4>{$pDB[$_GET['p']]['title']}</h4>";
	}
	?>
	<table class="table table-striped table-hover" id="mediaLibrary">
		<thead>
			<tr>
				<th>Title</th>
				<th>Artist</th>
				<th>Time</th>
				<th>Description</th>
				<!--<th>Uploader</th>-->
			</tr>
		</thead>
		<tbody id="catalog">
		</tbody>
	</table>
</div>

<audio id="mediaPlayer" style="display: none;"></audio>
<div id="mediaPlayerContainer">
	<span class='fa fa-plus optional' id='hAdd' data-featherlight='?app=tokyo-ajax&prompt=upload'></span>
	<span class='fa fa-download' id='hDL'></span>
	<span class='fa fa-play' id='mPlayPause'></span>
	<input type='range' id='mSeeker' min='0' max='1' style='width: 200px; display: inline-block;' />
	<span id='tPosition'>0:00</span>
	<span class='fa fa-forward' id='hForward'></span>
	&nbsp;&nbsp;
	<span class='fa fa-volume-down optional' id='hVolDown'></span>
	<input type='range' id='mVol' min='0' max='1' step='0.01' value='1' style='width: 64px; display: inline-block;' />
	<span class='fa fa-volume-up' id='hVolUp'></span>
	&nbsp;&nbsp;
	<span class='fa fa-repeat optional' id='sRepeat'></span>
	<span class='fa fa-random optional' id='sShuffle'></span>
	<span class='fa fa-play-circle optional' id='sAutoplay'></span>
</div>
<div style="display: none;">
	<div id="errMsg" style="text-align: center;"></div>
</div>

<?php
if (isset($_GET["gp"])) {
	$db = readDB("data/playlist.db");
	$hash = substr(sha1(time() . $_GET["gp"]), 0, 10);
	$db[$hash] = array(
		"title" => $_GET["gp"],
		"media" => array()
	);
	writeDB("data/playlist.db", $db);
	echo "<script>location.href = '?app=Tokyo&p={$_GET['hash']}';</script>";
}
?>

<script>
	var audioDOM = document.getElementById("mediaPlayer");
	var audio = $("#mediaPlayer");
	var active = "";
	var newQueue = "";
	var playlist = "<?php if (isset($_GET["p"])) echo $_GET["p"]; ?>";
	
	function str_pad_left(string, pad, length) {
		return (new Array(length+1).join(pad)+string).slice(-length);
	}
	function formatTime(value) {
		var time = Math.floor(value);
		var minutes = Math.floor(time / 60);
		var seconds = time - (minutes * 60);
		var ctime = minutes + ':' + str_pad_left(seconds,'0',2);
		return ctime;
	}
	function initUpload(url, title, artist, desc) {
		$.notify("Uploading: " + title + " - " + artist, { position: "bottom right", className: "info" });
		$.post("?app=tokyo-ajax&prompt=upload2", { url: url, title: title, artist: artist, desc: desc, author: "<?php echo AUTH_USER; ?>" }).done(function(data) {
			$.notify(data, { position: "bottom right", className: "info" });
			refreshList();
		});
	}
	function refreshList() {
		$("#catalog").load("?app=tokyo-ajax&prompt=catalog&p=" + playlist, function() {
			$("#mediaLibrary").tablesorter();
			$("#mediaLibrary").filterTable({ label: "", containerTag: "div", containerClass: "form-group", placeholder: "Search..." });
			$("input[type=search]").addClass("form-control");
		});
	}
	
	function playMedia(id) {
		active = id;
		location.hash = id;
		resetPlayButtons();
		setPlayButton();
		$("#mediaPlayer").attr("src", "data/tokyo/" + id + ".mp3");
		audio[0].pause();
		audio[0].load();
		audio[0].oncanplaythrough = audio[0].play();
		// scroll
		$('html, body').animate({
			scrollTop: $(".playBtn[media-id=" + active + "]").offset().top - 100
		}, 500);
		// highlight
		$("#mediaLibrary tr.info").removeClass("info");
		$(".playBtn[media-id=" + active + "]").parent().parent().addClass("info");
	}
	function playNextMedia() {
		if (active == "" || active == undefined) return;
		if ($('#optAutoplay').is(':checked')) {
			var next = active;
			if ($('#optRepeat').is(':checked')) {
				// do nothing, repeat.
			}
			else if ($('#optShuffle').is(':checked') && $(".playBtn").length > 1) {
				do {
					next = $("#mediaLibrary tr:visible").random().find(".playBtn").attr("media-id");
				}
				while (next == active); // so it doesn't play same song.
			}
			else {
				next = $(".playBtn[media-id=" + active + "]").parent().parent().nextAll("tr:visible").first().find(".playBtn").attr("media-id");
			}
			if (next == "" || next == undefined) return; // lazy catch for end.
			playMedia(next);
		}
	}
	function setPlayButton() {
		//play
		if (active == "") return;
		$(".playBtn[media-id=" + active + "]").addClass("fa-pause").removeClass("fa-play");
		document.title = $(".aTxt[media-id=" + active + "][text-type=title]").text() + " - " + $(".aTxt[media-id=" + active + "][text-type=artist]").text() + " | tōkyō";
		$("#hTitle").html($(".aTxt[media-id=" + active + "][text-type=title]").text() + " - " + $(".aTxt[media-id=" + active + "][text-type=artist]").text());
	}
	function resetPlayButtons() {
		//pause
		$(".fa-pause").removeClass("fa-pause").addClass("fa-play");
		document.title = "tōkyō";
		$("#hTitle").html("tōkyō");
	}
	
	function finishLoad() {
		if (newQueue !== "") {
			playMedia(newQueue);
			newQueue = "";
		}
		setPlayButton();
	}
	
	$("#catalog").on("click", ".playBtn", function() {
		if (!audioDOM.paused && active == $(this).attr("media-id")) {
			audioDOM.pause();
		}
		else if (active == $(this).attr("media-id")) {
			audioDOM.play();
		}
		else {
			playMedia($(this).attr("media-id"));
		}
	});
	$("#catalog").on("dblclick", ".aTxt", function() {
		var aTxt = $(this).text().trim();
		var val = prompt("Change \"" + aTxt + "\" to...", aTxt);
		if (val !== null) {
			$.post("?app=tokyo-ajax&prompt=medit", { id: $(this).attr("media-id"), type: $(this).attr("text-type"), value: val }).done(function(data) {
				$.notify(data, { position: "bottom right", className: "info" });
				refreshList();
			});
		}
	});
	$("body").on("click", ".addPBtn", function() {
		$("#catalog").load("?app=tokyo-ajax&prompt=catalog&p=" + playlist + "&a=" + $(this).attr("media-id"));
		$(this).hide();
	});
	$("#mediaPlayer").on("ended", function() {
		playNextMedia();
	});
	$("#hForward").click(function() {
		playNextMedia();
	});
	$("#hDL").click(function() {
		if (active == "" || active == undefined) return;
		location.href = '?app=tokyo-ajax&prompt=dl&id=' + active;
	});
	$("#optRepeat").click(function() {
		if ($(this).is(":checked")) {
			$.notify("Song set to repeat", { position: "bottom right", className: "info" });
		}
		else {
			$.notify("Song will not repeat", { position: "bottom right", className: "info" });
		}
	});
	$("#optShuffle").click(function() {
		if ($(this).is(":checked")) {
			$.notify("Playlist set to shuffle", { position: "bottom right", className: "info" });
		}
		else {
			$.notify("Playlist will play in order", { position: "bottom right", className: "info" });
		}
	});
	$("#optAutoplay").click(function() {
		if ($(this).is(":checked")) {
			$.notify("Playlist will autoplay", { position: "bottom right", className: "info" });
		}
		else {
			$.notify("Playlist will not autoplay", { position: "bottom right", className: "info" });
		}
	});
	// pretty sliders
	$("#mVol").on("input", function() {
		audioDOM.volume = $(this).val();
	});
	$("#mSeeker").on("change", function() {
		audioDOM.pause();
		audioDOM.currentTime = $(this).val();
		audioDOM.play();
	});
	$("#mPlayPause").click(function() {
		if (audioDOM.paused) {
			audioDOM.play();
		}
		else {
			audioDOM.pause();
		}
	});
	$("#sRepeat").click(function() {
		$("#optRepeat").click();
	});
	$("#sShuffle").click(function() {
		$("#optShuffle").click();
	});
	$("#sAutoplay").click(function() {
		$("#optAutoplay").click();
	});
	
	function updateSeeker() {
		if (audioDOM.paused) {
			$("#mPlayPause").removeClass("fa-pause").addClass("fa-play");
		}
		else {
			$("#mPlayPause").removeClass("fa-play").addClass("fa-pause");
		}
		$("#mSeeker").prop("max", audioDOM.duration).val(audioDOM.currentTime);
		$("#tPosition").text(formatTime(audioDOM.currentTime));
		if ($("#optRepeat").is(":checked")) {
			$("#sRepeat").css("color", "#006687");
		}
		else {
			$("#sRepeat").css("color", "#ffffff");
		}
		if ($("#optShuffle").is(":checked")) {
			$("#sShuffle").css("color", "#006687");
		}
		else {
			$("#sShuffle").css("color", "#ffffff");
		}
		if ($("#optAutoplay").is(":checked")) {
			$("#sAutoplay").css("color", "#006687");
		}
		else {
			$("#sAutoplay").css("color", "#ffffff");
		}
	}
	
	$(function() {
		//initCheckLoginStatus();
		refreshList();
		if (location.hash.length == 12) {
			newQueue = location.hash.substring(1);
		}
		setInterval("updateSeeker()", 750);
		updateSeeker();
	});
	audio[0].onplay = function() { setPlayButton() };
	audio[0].onpause = function() { resetPlayButtons() };
	
	// extends
	
	$.fn.random = function() {
		return this.eq(Math.floor(Math.random() * this.length));
	}
	
	// ctrl-F hook
	$(window).keydown(function(e){
		if ((e.ctrlKey || e.metaKey) && e.keyCode === 70) {
			$("input[type='search']").focus();
			e.preventDefault();
		}
	});
</script>
