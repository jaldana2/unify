<?php
requireLogin();
define("PARENT_APP", "Urusai");

$urusai = new Urusai();
$g_anime = "";
$g_anime_id = "";
$g_episode = "";
if (isset($_GET["anime"])) {
	$g_anime_id = $_GET["anime"];
	$g_episode = $_GET["ep"];
	$g_anime = $urusai->getAnimeAttribute($g_anime_id, "title");
}

$rid = $_GET["room"];

if (isset($_SERVER["HTTP_X_FORWARDED_PROTO"]) && $_SERVER["HTTP_X_FORWARDED_PROTO"] == "https") {
	echo "
	<script>
		window.location = 'http://aftermirror.com{$_SERVER['REQUEST_URI']}';
	</script>
	";
}
else {

echo "
<style type='text/css'>
.theatre { width: 90%; max-width: 1200px; margin: auto; margin-bottom: 50px; }
.theatre video { width: 100%; max-height: 800px; }
.control { position: absolute; bottom: 5px; width: 100%; background-color: rgba(0, 0, 0, 0.35); padding: 2px 6px; opacity: 0; font-size: 1.2em; -webkit-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none; }
.underbar { width: 100%; background-color: black; color: white; padding: 2px 6px; }
.control .fa { margin-right: 4px; }
.control .fa:hover { color: #375A7F; }
.control .separator { margin-right: 16px; }
.video-wrapper { position: relative; color: white; }
.textpop { position: absolute; top: 20px; left: 0px; background-color: rgba(0, 0, 0, 0.7); padding: 2px 12px; box-shadow: -4px 0 0 #375A7F; max-width: 70%; display: none; }
.textpop p { padding: 0px; margin: 0px; font-size: 1.2em; overflow: hidden; white-space: no-wrap; text-overflow: ellipsis; }
.textpop span { font-size: 0.9em; }
.textlog { position: absolute; top: 0px; right: 0px; bottom: 54px; width: 25%; overflow: hidden; transition: background-color 0.5s ease; )}
.textlog:hover { background-color: rgba(0, 0, 0, 0.35); }
#log { padding: 10px; height: 100%; position: relative; }
#log li { list-style-type: none; word-wrap: break-word; }
#statusbar { width: 100%; opacity: 0.5; font-size: 0.9em; }
#statusbar:hover { opacity: 1; }
#statusbar span.uspan { display: inline-block; margin-right: 5px; padding: 0; padding-bottom: 4px; }
#statusbar span.fa { margin-left: 4px; }
#statusbar .profile-picture-sm { margin: 0; width: 80px; height: 80px; padding-bottom: 8px; }
#textinput { position: relative; top: -5px; border: none; outline: none; box-sizing: border-box; }
.debug { display: none; }
/*headers*/
.sync_title { font-size: 20px; }
select { background-color: transparent; border: none; }
select option { background-color: #222; color: white; border: none; }
/*fixes!*/
</style>

<div class='theatre'>
	<div style='margin-top: 20px; margin-bottom: 5px;'>
		<div style='float: left;'>
			<select id='header_anime'>
				";
				$listing = $urusai->getListing();
				natsort($listing);
				foreach ($listing as $anime_id => $anime_title) {
					$count = $urusai->getAnimeAttribute($anime_id, "num_episodes");
					$status = $urusai->getAnimeAttribute($anime_id, "status");
					$airing = $urusai->getAnimeAttribute($anime_id, "airing");
					if ($count < 0) continue;
					if ($status === 0) continue;
					if ($airing === 1) $anime_title .= " (airing)";
					
					if ($anime_id == $g_anime_id) {
						echo "<option value='{$anime_id}' selected>{$anime_title}</option>";
					}
					else {
						echo "<option value='{$anime_id}'>{$anime_title}</option>";
					}
				}
				echo "
			</select>
			<select id='header_episodes'>
			</select>
			<span class='btn btn-xs btn-primary' id='media-qload-btn'>Load</span>
		</div>
		<div style='float: right;'>
			<span class='btn btn-xs btn-primary debug' id='media-load-btn' data-featherlight='?app=urusaisync-ajax'>Manual Load</span>
			<span class='fa fa-gear' id='media-settings-btn' data-featherlight='?app=urusaisync2-ajax&settingsDialog'></span>&nbsp;&nbsp;
			<span class='fa fa-comment' id='media-chat-btn' onclick='chatPopup();'></span>
		</div>
		<br clear='both' />
	</div>
	<div class='theatre-wrapper'>
		<div class='secondary-wrapper'>
			<div class='video-wrapper'>
				<video id='media' preload><source src='#' type='video/mp4' /></video>
				<div id='textpop' class='textpop'>
					<p id='textpop_txt'>Hello, testing!</p>
					&mdash; <span id='textpop_auth'>kuru!</span>
				</div>
				<div class='textlog' id='textlogctr'>
					<ul id='log'></ul>
				</div>
				<div class='control'>
					<input type='range' id='ctl_seeker' value='0' max='1' />
					<span class='separator'></span>
					<span class='fa fa-play' id='ctl_play'></span>
					<span class='fa fa-pause' id='ctl_pause'></span>
					<span class='separator'></span>
					<span class='fa fa-volume-off' id='ctl_vol_mute'></span>
					<span class='fa fa-volume-down' id='ctl_vol_down'></span>
					<span id='ctl_vol'>100%&nbsp;</span>
					<span class='fa fa-volume-up' id='ctl_vol_up'></span>
					<span class='separator'></span>
					<span id='ctl_time_label'>0:00 / 0:00</span>
					<!--
					<span class='separator'></span>
					<span id='room_title'>room {$rid}</span>
					-->
				</div>
			</div>
			<input type='text' id='textinput' class='underbar' placeholder='say something...; type /? for commands' />
		</div>
		<div id='statusbar'></div>
	</div>
</div>
<div style='display: none;'>
	<div id='ready-check' style='color: black; text-align: center;'>
		<h2 style='color: black;'>Ready?</h2>
		<span id='ready-check-yes' class='btn btn-lg btn-primary'>Yes &nbsp;&nbsp;<span class='fa fa-thumbs-up'></span></span> 
		<span id='ready-check-no' class='btn btn-lg btn-danger'>No &nbsp;&nbsp;<span class='fa fa-thumbs-down'></span></span>
	</div>
	<audio id='beep' preload>
		<source src='sounds/beep.mp3' type='audio/mp3' />
	</audio>
</div>

<script>
	var media_vol = 10;
	var hide_timer = false;
	var name = '" . AUTH_USER . "';
	if (Cookies.get('chatname')) {
		name = Cookies.get('chatname');
	}
	var room = '{$rid}';
	var liveuser = [];
	
	var stat_player_playing = false;
	var stat_player_loaded = false;
	var stat_chat_typing = false;
	var stat_chat_focused = false;
	var prefer_hd = false;
	
	var ready_check = false;
	
	var s_ep = {$_GET['ep']};
	var first_load_init = false;
	var chat_delay = 20000;
	var chat_on = false;
	function chatPopup() {
		if (chat_on) {
			chat_on = false;
			$('#log li').clearQueue().fadeOut(200);
		}
		else {
			chat_on = true;
			$('#log li').clearQueue().fadeIn(200);
			$('#textlogctr').scrollTop($('#textlogctr').scrollTop() + $('#log li:last').position().top);
		}
	}
	$('#beep')[0].volume = 0.25;
	$(function() {
		media_vol = 4;
		setVolume(4);
	});
</script>
<script src='dist/js/sync.js'></script>
";

}
?>