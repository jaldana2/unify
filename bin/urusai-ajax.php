<?php
enforceLogin();

if (isset($_GET["banner"])) {
	$id = $_GET["banner"];
	if (file_exists("data/urusai/banner/{$id}.jpg")) {
		header("Content-Type: image/jpg");
		readfile("data/urusai/banner/{$id}.jpg");
	}
	else {
		header("Content-Type: image/jpg");
		readfile("data/urusai/banner/default.jpg");
	}
}
elseif (isset($_GET["poster"])) {
	$id = $_GET["poster"];
	if (file_exists("data/urusai/poster/{$id}.jpg")) {
		header("Content-Type: image/jpg");
		readfile("data/urusai/poster/{$id}.jpg");
	}
	else {
		header("Content-Type: image/jpg");
		readfile("data/urusai/poster/default.jpg");
	}
}
elseif (isset($_GET["eid"])) {	
	$urusai = new Urusai();
	$data = $urusai->getEpisode($_GET["eid"]);
	echo "
		<video controls style='max-width: 100%;'>
			<source src='{$data['src']}' type='video/mp4' />
		</video>
	";	
}
?>