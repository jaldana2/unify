<?php
switch ($_GET["do"]) {
	case "login":
		if (isset($_GET["check-username"])) {
			if (!isset($auth)) $auth = new Auth();
			if (!$auth->checkUserExists($_GET["check-username"])) {
				echo json_encode(array(
					"valid" => "no",
					"password" => "no",
					"banned" => "no"
					));
			}
			elseif ($auth->getUserAccess($_GET["check-username"]) == AUTH_ACCESS_BANNED) {
				echo json_encode(array(
					"valid" => "no",
					"password" => "no",
					"banned" => "yes"
					));
			}
			elseif ($auth->checkIfPasswordless($_GET["check-username"])) {
				echo json_encode(array(
					"valid" => "yes",
					"password" => "no",
					"banned" => "no",
					"xu" => base64_encode(fnEncrypt($_GET["check-username"])),
					"xp" => base64_encode(fnEncrypt(""))
					));
			}
			else {
				if (isset($_GET["check-password"]) && isSomething($_GET["check-password"])) {
					$auth = new Auth();
					if ($auth->getUserPassword($_GET["check-username"]) == $auth->getNewPasswordHash($_GET["check-password"])) {
						echo json_encode(array(
							"valid" => "yes",
							"password" => "yes",
							"banned" => "no",
							"xu" => base64_encode(fnEncrypt($_GET["check-username"])),
							"xp" => base64_encode(fnEncrypt($_GET["check-password"])),
							"success" => "yes"
							));
					}
					else {
						echo json_encode(array(
							"valid" => "yes",
							"password" => "yes",
							"banned" => "no",
							"success" => "no"
							));
					}
				}
				else {
					echo json_encode(array(
						"valid" => "yes",
						"password" => "yes",
						"banned" => "no"
						));	
				}
			}
		}
	break;
	case "journal-page-init":
		if (!defined("AUTH_USER")) {
			// something went wrong / login expired
			
		}
		else {
			
		}
	break;
}
?>