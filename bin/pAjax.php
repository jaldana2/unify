<?php
switch ($_GET["do"]) {
	case "Notifications":
		$notify = new Notify(AUTH_USER);
		$countUR = $notify->countUnreadNotifications();
		echo "
			<li class='header'>You have " . $countUR . " notification(s)</li>
		";
		echo "
			<li>
				<ul class='menu'>
		";
		$gn = $notify->getNotifications();
		$gn = array_slice(array_reverse($gn, true), 0, 10, true);
		foreach ($gn as $id => $n) {
			echo "
				<li>
					<a href='#' data-featherlight='?app=pAjax&do=quickNotificationPopup&id={$id}'>
						<i class='fa {$n['icon']}'></i> <b>{$n['title']}</b><br/>{$n['content']}
					</a>
				</li>
			";
		}
		echo "
				</ul>
			</li>
		";
		echo "
			<li class='footer'><a href='?app=Notifications'>View all</a></li>
		";
		/*
		  <li>
			<!-- Inner Menu: contains the notifications -->
			<ul class='menu'>
			  <li><!-- start notification -->
				<a href='#'>
				  <i class='fa fa-users text-aqua'></i> 5 new members joined today
				</a>
			  </li>
			  <!-- end notification -->
			</ul>
		  </li>
		*/
	break;
	case "Messages":
		$notifyMail = new Notify(AUTH_USER . ".mail");
		echo "
		  <li class='header'>
			<div class='pull-left'>You have " . $notifyMail->countUnreadNotifications() . " message(s)</div>
			<div class='pull-right'><a href='#' class='btn btn-xs btn-primary' data-featherlight='?app=pAjax&do=quickMessage'>Quick Message</a></div>
			<br />
		  </li>
		  <li>
			<!-- inner menu: contains the messages -->
			<ul class='menu'>
			";
			$gn = $notifyMail->getNotifications();
			$gn = array_slice(array_reverse($gn, true), 0, 10, true);
			foreach ($gn as $id => $n) {
				$sp = $n["content"];
				if (strlen($sp) > 64) { $sp = substr($sp, 0, 61) . "..."; }
				if (!$n["_read"]) {
					$sp = "<b>{$sp}</b>";
				}
				echo "
					<li>
						<a href='?app=Messages&do=read&msg={$id}' data-featherlight='?app=pAjax&do=quickRead&msg={$id}'>
							<div class='pull-left'>
								<div class='img-circle-div-sm' style='width: 40px; height: 40px; background-image: url(?app=img&do=profile_picture&v=" . $n["sender"] . ");'></div>
							</div>
							<h4>
								" . $n["sender"] . "
								<small><i class='fa fa-clock-o'></i> " . time_since(time() - $n["_sent"]) . "</small>
							</h4>
							<p>{$sp}</p>
						</a>
					</li>
				";
			}
			echo "
			</ul>
			<!-- /.menu -->
		  </li>
		  <li class='footer'><a href='?app=Messages'>See All Messages</a></li>
		";
	break;
	case "quickRead":
		$notifyMail = new Notify(AUTH_USER . ".mail");
		$mail = $notifyMail->getNotification($_GET["msg"]);
		$notifyMail->readNotification($_GET["msg"]);
		echo "
			<div class='box'>
				<div class='box-header with-border'>
					<h3 class='box-title'>{$mail['sender']}</h3>
				</div>
				<div class='box-body'>
					<p>{$mail['content']}</p>
				</div>
				<div class='box-footer'>
					<a href='?app=Messages&del={$_GET['msg']}' onclick=\"$.featherlight.current().close(); $.post('?app=Messages&del={$_GET['msg']}').done(function(e) { $.notify('Message deleted.', { position: 'bottom right', className: 'info' }); }); return false;\" class='btn btn-sm btn-danger'>Delete</a>
					<a href='?app=Messages&do=compose&reply={$mail['sender']}' class='btn btn-sm btn-info'>Reply</a>
				</div>
			</div>
		";
	break;
	case "quickMessage":
		echo "
			<div class='box box-primary'>
				<div class='box-header with-border'>
					<h3 class='box-title'>New Message</h3>
				</div>
				<form role='form' action='?app=Messages&do=compose' method='post' id='qMsg'>
					<div class='box-body'>
						<div class='form-group'>
							<label for='msgRecipient'>To</label>
							<select name='msgRecipient' id='msgRecipient' class='form-control'>
								<option value='NO_RECIPIENT'>Select recipient...</option>
		";
		$users = $auth->getAllUsers();
		natsort($users);
		foreach ($users as $user) {
			if (!isSomething($user)) continue;
			if (isset($_GET["reply"]) && $user == $_GET["reply"]) {
				echo "<option value='{$user}' selected>{$user}</option>";
			}
			else {
				echo "<option value='{$user}'>{$user}</option>";
			}
		}
		echo "
							</select>
						</div>
						<div class='form-group'>
							<label for='msgContent'>Message</label>
							<textarea name='msgContent' id='msgContent' class='form-control' rows='3'></textarea>
						</div>
						<div class='form-group'>
							<input type='submit' value='Send' class='btn btn-primary form-control' />
						</div>
					</div>
				</form>
			</div>
			<div style='display: none;'>
				<script>
					$(function() {
						$('#msgContent').autoGrow();
						$('#qMsg').submit(function(e) {
							if ($('#msgRecipient').val() == 'NO_RECIPIENT') {
								alert('Please enter a valid recipient.');
							}
							else {
								$.post('?app=Messages&do=compose', { msgRecipient: [$('#msgRecipient').val()], msgContent: $('#msgContent').val() }).done(function(data) {
									$.notify('Message sent!', { position: 'bottom right', className: 'success' });
								});
								$.featherlight.current().close();
							}
							e.preventDefault();
						});
					});
				</script>
			</div>
		";
	break;
	case "quickNotificationPopup":
		$notify = new Notify(AUTH_USER);
		$notification = $notify->getNotification($_GET["id"]);
		$notify->readNotification($_GET["id"]);
		echo "
			<div class='box'>
				<div class='box-header with-border'>
					<h3 class='box-title'>{$notification['title']}</h3>
				</div>
				<div class='box-body'>
					<p>{$notification['content']}</p>
		";
		if (isset($notification["link"])) {
			echo "
				<p><a href='{$notification['link']}' class='btn btn-sm btn-primary'>{$notification['link-title']}</a></p>
			";
		}
		echo "
				</div>
			</div>
		";
	break;
}
?>