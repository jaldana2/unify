<?php
switch ($_GET["do"]) {
	case "login":
		$username = $_GET["username"];
		$password = $_GET["password"];
		$username = fnDecrypt(base64_decode($username));
		$password = fnDecrypt(base64_decode($password));
		
		$auth = new Auth();
		if ($auth->getUserPassword($username) == $auth->getNewPasswordHash($password)) {
			// password match (includes password-less accounts)
			$session = $auth->getUserSessionKey($username);
			setcookie("auth", $session, time() + (3600 * 24 * 30));
			
			if (isSomething($_GET["return"])) {
				header("Location: ?app=passthru&site=welcome&qs=" . $_GET["return"]);
			}
			else {
				header("Location: ?app=Home&do=welcome");
			}
		}
		else {
			if (isSomething($_GET["return"])) {
				header("Location: ?app=Login&do=login&username={$username}&qs={$_GET['return']}&error=" . AUTH_ERROR_CRYPT_INVALID_ERROR);
			}
			else {
				header("Location: ?app=Login&do=login&username={$username}&error=" . AUTH_ERROR_CRYPT_INVALID_ERROR);
			}
		}
	break;
	case "logout":
		setcookie("auth", "", time() - 3600);
		session_start();
		session_destroy();
		header("Location: ?app=Home&do=goodbye");
	break;
}
?>