<?php
$do = "login";
if (isset($_GET["do"])) $do = $_GET["do"];

switch($do) {
	case "proc-register":
		$auth = new Auth();
		$protectedUsers = array(
			"admin",
			"administrator",
			"webmaster",
			"postmaster",
			"system",
			"sys",
			"debug",
			"help",
			"support"
		);
		if (cleanANString(strtolower($_POST["username"])) !== strtolower($_POST["username"])) {
			echo "
				<h3>Please enter a valid username. Only a-z and 0-9.</h3>
				<a href='?app=Register&do=register' class='btn btn-lg btn-danger'>Return</a>
			";
		}
		elseif (in_array(strtolower($_POST["username"]), $protectedUsers)) {
			echo "
				<h3>You cannot use this username. Please try something else.</h3>
				<a href='?app=Register&do=register' class='btn btn-lg btn-danger'>Return</a>
			";
		}
		else {
			$act = $auth->registerNewUser(strtolower($_POST["username"]), $_POST["password"]);
			switch ($act) {
				case AUTH_ERROR_NO_ERROR:
					echo "
					<h3>Successfully registered.</h3>
					<a href='?app=Login&do=login' class='btn btn-lg btn-success'>Login</a>
					";
				break;
				case AUTH_ERROR_USER_EXISTS:
					echo "
					<h3>Username exists.</h3>
					<a href='?app=Register&do=register' class='btn btn-lg btn-danger'>Return</a>
					";
				break;
				default:
				case AUTH_ERROR_GENERIC:
					echo "
					<h3>Unknown error: {$act}</h3>
					<a href='?app=Register&do=register' class='btn btn-lg btn-danger'>Return</a>
					";
				break;
			}
		}
		break;
	case "register":
		echo "
			<h3>Register</h3>
			<p>
				<b>TL;DR readme</b>
				<br />
				<ul>
					<li>It's just another random website you signed up for, huh.</li>
				</ul>
			</p>
			<form action='?app=Register&do=proc-register' method='post'>
				<div class='form-group'>
					<label>Username <span style='color: red;'>*</span></label>
					<input type='text' name='username' placeholder='Username' class='form-control' />
					<small>Only [a-z] and [0-9] characters are allowed.</small>
				</div>
				<div class='form-group'>
					<label>Password (optional) &mdash; Yes, you can have a password-less account.</label>
					<input type='password' name='password' placeholder='Password' class='form-control' />
				</div>
				<div class='form-group'>
					<input type='submit' value='Register' class='btn btn-warning form-control' />
				</div>
			</form>
		";
	break;
}
?>