<?php
requireLogin();

echo "
	<a href='?app=Messages&do=compose' class='btn btn-primary'><i class='fa fa-edit'></i> New Message</a>
	<br />
	<br />
";

if (isset($_GET["do"])) {
	switch ($_GET["do"]) {
		case "compose":
			if (isSomething($_POST["msgRecipient"]) && isSomething($_POST["msgContent"])) {
				foreach ($_POST["msgRecipient"] as $rec) {
					$notifyMail = new Notify($rec . ".mail");
					$notifyMail->addNotification(array(
						"content" => htmlentities($_POST["msgContent"]),
						"sender" => AUTH_USER
					));
				}
				echo "
				<div class='callout callout-success'>
					<h4>Success!</h4>
					<p>Your message has been sent.</p>
				</div>
				";
			}
			echo "
				<div class='box box-primary'>
					<div class='box-header with-border'>
						<h3 class='box-title'>New Message</h3>
					</div>
					<form role='form' action='?app=Messages&do=compose' method='post'>
						<div class='box-body'>
							<div class='form-group'>
								<label for='msgRecipient'>To</label>
								<select name='msgRecipient[]' id='msgRecipient' class='form-control' multiple='multiple'>
			";
			$users = $auth->getAllUsers();
			natsort($users);
			foreach ($users as $user) {
				if (isset($_GET["reply"]) && $user == $_GET["reply"]) {
					echo "<option value='{$user}' selected>{$user}</option>";
				}
				else {
					echo "<option value='{$user}'>{$user}</option>";
				}
			}
			echo "
								</select>
							</div>
							<div class='form-group'>
								<label for='msgContent'>Message</label>
								<textarea name='msgContent' id='msgContent' class='form-control' rows='3'></textarea>
							</div>
							<div class='form-group'>
								<input type='submit' value='Send' class='btn btn-primary form-control' />
							</div>
						</div>
					</form>
				</div>
				<script>
					$(function() {
						$('#msgRecipient').select2();
						$('#msgContent').autoGrow();
					});
				</script>
			";
		break;
		case "read":
			$notifyMail = new Notify(AUTH_USER . ".mail");
			$mail = $notifyMail->getNotification($_GET["msg"]);
			$notifyMail->readNotification($_GET["msg"]);
			echo "
				<div class='box'>
					<div class='box-header with-border'>
						<h3 class='box-title'>{$mail['sender']}</h3>
					</div>
					<div class='box-body'>
						<p>{$mail['content']}</p>
					</div>
					<div class='box-footer'>
						<a href='?app=Messages&del={$_GET['msg']}' class='btn btn-sm btn-danger'>Delete</a>
						<a href='?app=Messages&do=compose&reply={$mail['sender']}' class='btn btn-sm btn-info'>Reply</a>
					</div>
				</div>
			";
		break;
	}
}
else {
	echo "
		<div class='box'>
			<div class='box-header with-border'>
				<h3 class='box-title'>Messages</h3>
			</div>
			<div class='box-body'>
				<table class='table table-bordered'>
				<tr>
					<th style='width: 64px;'></th>
					<th style='width: 120px;'>Sender</th>
					<th>Message</th>
				</tr>
	";
	$notifyMail = new Notify(AUTH_USER . ".mail");
	if (isset($_GET["del"])) {
		$notifyMail->removeNotification($_GET["del"]);
	}
	foreach ($notifyMail->getNotifications() as $id => $n) {
		$sp = $n["content"];
		if (strlen($sp) > 64) { $sp = substr($sp, 0, 61) . "..."; }
		echo "
			<tr>
				<td style='text-align: center;'><a href='?app=Messages&do=read&msg={$id}'>View</a></td>
				<td>{$n['sender']}</td>
				<td>{$sp}</td>
			</tr>
		";
	}
	echo "
				</table>
			</div>
		</div>
	";
}
?>