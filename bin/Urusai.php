<?php
requireLogin();
define("PARENT_APP", "Urusai");

echo "
	<div class='box'>
		<div class='box-header'>
			<h4 class='box-title'>Options</h4>
		</div>
		<div class='box-body'>
			<label for='btn-filter-rplus' class='btn btn-danger'><input type='checkbox' id='btn-filter-rplus' checked /> R+</label> 
			<label for='btn-filter-r17plus' class='btn btn-warning'><input type='checkbox' id='btn-filter-r17plus' checked /> R-17+</label>
			<br />
			<br />
			<div class='form-group'>
				<div class='input-group'>
					<input type='text' id='search' placeholder='Search...' class='form-control' autocomplete='off' />
					<span class='input-group-btn'>
						<button class='btn btn-primary' type='btn'><span class='fa fa-search'></span></button>
					</span>
				</div>
			</div>
		</div>
	</div>
";

$urusai = new Urusai();
$gl = $urusai->getListing();
natsort($gl);

echo "
	<div class='row'>
	<style type='text/css'>
		.anime-poster { height: 300px; background-size: cover; background-position: center center; transition: box-shadow 0.25s; position: relative; cursor: pointer; }
		.anime-poster:hover { box-shadow: 0px 0px 8px black, 0px 0px 4px black; }
		.anime-title { position: absolute; bottom: 0; left: 0; right: 0; padding: 8px; background-color: rgba(0, 0, 0, 0.75); color: white; }
		.anime-desc { position: absolute; top: 0; left: 0; right: 0; bottom: 0; padding: 10px; background-color: rgba(0, 0, 0, 0.75); color: white; opacity: 0; transition: opacity 0.25s; overflow: hidden; }
		.anime-desc:hover { opacity: 1; }
		.ani-hidden { display: none; }
		.anime-alt-title { color: #999999; font-size: 0.75em; }
	</style>
";

foreach ($gl as $id => $title) {
	$data = $urusai->getAnimeAttributes($id);
	$extraBtns = "";
	$descText = "";
	$ratingDot = "";
	$extraQuery = "";
	$ratingClass = "pg";
	$altTitle = "";
	if (strc($data["rating"], "R+")) {
		$descText .= "<span class='label label-danger'><i class='fa fa-warning'></i> {$data['rating']}</span><br />";
		$ratingDot = "<i class='fa fa-circle text-red'></i>";
		$ratingClass = "rplus";
	}
	elseif (strc($data["rating"], "R")) {
		$descText .= "<span class='label bg-orange'><i class='fa fa-warning'></i> {$data['rating']}</span><br />";
		$ratingDot = "<i class='fa fa-circle text-orange'></i>";
		$ratingClass = "r17plus";
	}
	else {
		$descText .= "<span class='label bg-teal'><i class='fa fa-warning'></i> {$data['rating']}</span><br />";
		$ratingDot = "<i class='fa fa-circle text-teal'></i>";
	}
	if ($data["status"] === 0) {
		$extraBtns .= "<span class='label label-danger'><i class='fa fa-close'></i> Not Available</span> ";
		$extraQuery .= " not available ";
	}
	if ($data["airing"] === 1) {
		$extraBtns .= "<span class='label label-primary'><i class='fa fa-clock-o'></i> Airing</span> ";
		$extraQuery .= " airing ";
	}
	elseif ($data["airing"] === 2) {
		$extraBtns .= "<span class='label label-primary'><i class='fa fa-clock-o'></i> Scheduled</span> ";
		$extraQuery .= " scheduled ";
	}
	if (strlen($extraBtns) > 0) $extraBtns .= "<br/>";
	if (isSomething($data["genres"])) {
		foreach (explode(", ", $data["genres"]) as $genre) {
			$descText .= "<span class='label label-primary'>{$genre}</span> ";
		}
		$descText .= "<br/>";
	}
	if (isSomething($data["synopsis"])) {
		if (strlen($data["synopsis"]) > 380) {
			$shortText = substr($data["synopsis"], 0, 380);
			$shortText = substr($shortText, 0, strripos($shortText, " ")) . "...";
			$descText .= "<small>" . $shortText . "</small>";
		}
		else {
			$descText .= "<small>" . $data["synopsis"] . "</small>";
		}
	}
	if (isSomething($data["alt_title"])) {
		$altTitle = "<br /><span class='anime-alt-title'>{$data['alt_title']}</span>";
	}
	
	if ($data["airing"] === 2) {
		echo "
			<div class='col-xs-6 col-sm-4 col-md-2 ani-block ani-rating-{$ratingClass}' search-query=\"{$extraQuery} {$data['title']} {$data['alt_title']} {$data['genres']}\">
				<div class='anime-poster anime-future' data-href='#' style='background: repeating-linear-gradient(45deg, rgba(255, 255, 0, 0.4), rgba(255, 255, 0, 0.4) 10px, rgba(0, 0, 0, 0.4) 10px, rgba(0, 0, 0, 0.4) 20px), url(?app=urusai-ajax&poster={$id});'>
					<div class='anime-desc'>
					{$descText}
					</div>
					<span class='anime-title'>{$extraBtns}{$ratingDot} {$data['title']}{$altTitle}</span>
				</div>
				<br />
			</div>
		";
	}
	elseif ($data["status"] === 0) {
		echo "
			<div class='col-xs-6 col-sm-4 col-md-2 ani-block ani-unavailable ani-rating-{$ratingClass}' search-query=\"{$extraQuery} {$data['title']} {$data['alt_title']} {$data['genres']}\">
				<div class='anime-poster' data-href='#' style='background-image: url(?app=urusai-ajax&poster={$id}); opacity: 0.5;'>
					<div class='anime-desc'>
					{$descText}
					</div>
					<span class='anime-title'>{$extraBtns}{$ratingDot} {$data['title']}{$altTitle}</span>
				</div>
				<br />
			</div>
		";
	}
	else {
		echo "
			<div class='col-xs-6 col-sm-4 col-md-2 ani-block ani-rating-{$ratingClass}' search-query=\"{$extraQuery} {$data['title']} {$data['alt_title']} {$data['genres']}\">
				<div class='anime-poster' data-href='?app=UrusaiPlayer&anime={$id}' style='background-image: url(?app=urusai-ajax&poster={$id});'>
					<div class='anime-desc'>
					{$descText}
					</div>
					<span class='anime-title'>{$extraBtns}{$ratingDot} {$data['title']}{$altTitle}</span>
				</div>
				<br />
			</div>
		";
	}
	
	//break;
}

echo "
	</div>
";

echo "
<script>
	$('#btn-filter-rplus').click(function(e) {
		if ($(this).is(':checked')) {
			$('.ani-rating-rplus').removeClass('ani-hidden');
		}
		else {
			$('.ani-rating-rplus').addClass('ani-hidden');
		}
	});
	$('#btn-filter-r17plus').click(function(e) {
		if ($(this).is(':checked')) {
			$('.ani-rating-r17plus').removeClass('ani-hidden');
		}
		else {
			$('.ani-rating-r17plus').addClass('ani-hidden');
		}
	});
	$('.anime-poster').click(function(e) {
		if ($(this).attr('data-href') !== '#') location.href = $(this).attr('data-href');
	});
	$('#search').on('keyup', function(e) {
		query = $('#search').val().toLowerCase();
		console.log(query);
		if (query.length > 0) {
			$('.ani-block').each(function(e) {
				if ($(this).attr('search-query').toLowerCase().indexOf(query) >= 0) {
					$(this).fadeIn();
				}
				else {
					$(this).fadeOut(100);
				}
			});
		}
		else {
			$('.ani_block').show();
		}
	});
</script>
";
?>