<?php
set_time_limit(0);
define("ENGINE_PLUGIN_DIR", "e-plugins");
include "config/config.php";
include "engine.php";

$u = readDB("data/urusai.db");
$urusai = new Urusai();
print_a($u);
foreach ($u["listing"] as $id => $title) {
	$data = $u["data"][$id];
	$urusai->addAnime(
		$title,
		$data["alt_title"],
		"",
		$data["count"],
		implode(", ", $data["genre"]),
		$data["rating"],
		$data["synopsis"],
		0,
		1
	);
}
foreach ($u["episodes"] as $id => $data) {
	// shift id up.
	$anime_id = $id + 1;
	foreach ($data as $episode => $src) {
		if (isset($src["src_low"]) && strlen($src["src_low"]) > 0) {
			$urusai->addEpisode($anime_id, $episode, "SD", $src["src_low"]);
		}
		if (isset($src["src_high"]) && strlen($src["src_high"]) > 0) {
			$urusai->addEpisode($anime_id, $episode, "HD", $src["src_high"]);
		}
		if (isset($src["src_high_1080"]) && strlen($src["src_high_1080"]) > 0) {
			$urusai->addEpisode($anime_id, $episode, "HD1080", $src["src_high_1080"]);
		}
	}
}
for ($i = count($u["listing"]); $i > 0; $i--) {
	$e = $i - 1;
	if (file_exists("data/urusai/banner/{$e}.jpg")) {
		rename("data/urusai/banner/{$e}.jpg", "data/urusai/banner/{$i}.jpg");
	}
	if (file_exists("data/urusai/poster/{$e}.jpg")) {
		rename("data/urusai/poster/{$e}.jpg", "data/urusai/poster/{$i}.jpg");
	}
}
?>